#!/bin/bash

PROGPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$PROGPATH"

pio run -t erase && pio run -t uploadfs && pio run -e nathan -t upload && pio device monitor