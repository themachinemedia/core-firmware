
settings data

the individual entities of a product can be triggered by any of the following topics
- my_home/set/device_id
- my_home/set/group_id
- my_home/set/all_device_types

state devices respond on topic for state changes:
- my_home/status/device_type
  - [retain flag true for states, false for one-shot events]

Connected status
Each interface should maintain a topic.

- toplevelname/connected


MQTT Autodiscovery Formatting


<discovery_prefix>/<component>/[<node_id>/]<object_id>/config

where:
- <discovery_prefix> = the discovery topic
- <component> = one of the supported components (e.g. binary_sensor); see full list: https://www.home-assistant.io/docs/mqtt/discovery/
- <node_id> (Optional): ID of the node providing the topic, this is not used by Home Assistant but may be used to structure the MQTT topic.
- <object_id>: The ID of the device (not the same as entity_id). The ID of the device must only consist of characters from the character class [a-zA-Z0-9_-] (alphanumerics, underscore and hyphen).

and payload of:

{
  "~": "homeassistant/light/kitchen",
  "name": "Kitchen",
  "unique_id": "kitchen_light",
  "cmd_t": "~/set",
  "stat_t": "~/state",
  "schema": "json",
  "brightness": true
}

MQTT Formatting tips: https://github.com/mqtt-smarthome/mqtt-smarthome/blob/master/Architecture.md

For MQTT auto discovery info: https://www.home-assistant.io/docs/mqtt/discovery/