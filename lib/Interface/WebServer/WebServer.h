#ifndef WEB_SERVER_H
#define WEB_SERVER_H

#include <Arduino.h>
#include <AsyncTCP.h>
#include <DNSServer.h>
#include <Debug.h>
#include <ESPAsyncWebServer.h>
#include <IPAddress.h>

#ifndef STA_NAME
#define STA_NAME "Testing OTA"
#endif

using namespace std::placeholders;

class WebServerClass {
 private:
  /* Instances */
  DNSServer dnsServer;
  AsyncWebServer server;
  DebugClass Debug;

  /* Methods */

  /** @brief handle Not Found page */
  void handleNotFound(AsyncWebServerRequest* request);

  /** @brief handle Root */
  void handleRoot(AsyncWebServerRequest* request);

  /** @brief handle JavaScript */
  void handleJS(AsyncWebServerRequest* request);

  /** @brief handle CSS */
  void handleStyle(AsyncWebServerRequest* request);

  /** @brief handle WiFi Image */
  void handleWiFiImg(AsyncWebServerRequest* request);

  /** @brief handle Lock Image */
  void handleLockImg(AsyncWebServerRequest* request);

 public:
  /* Constructor */
  WebServerClass() : server(80), Debug("WebServer"){};

  /* Methods */
  /** @brief Setup the WebServer */
  void initialize();

  void DnsLoop();
};

extern WebServerClass WebServer;

#endif