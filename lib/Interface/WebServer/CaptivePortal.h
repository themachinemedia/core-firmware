#ifndef CAPTIVE_PORTAL_H
#define CAPTIVE_PORTAL_H

#include <Arduino.h>
#include <DNSServer.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Debug.h>
#include <FileStream.h>
#include <IPAddress.h>

class CaptiveRequestHandler : public AsyncWebHandler, DebugClass {
 public:
  CaptiveRequestHandler() : DebugClass("Captive Portal") {}
  virtual ~CaptiveRequestHandler() {}

  bool canHandle(AsyncWebServerRequest *request) {
    //request->addInterestingHeader("ANY");
    return true;
  }

  String toStringIp(IPAddress ip) {
    String res = "";
    for (int i = 0; i < 3; i++) {
      res += String((ip >> (8 * i)) & 0xFF) + ".";
    }
    res += String(((ip >> 8 * 3)) & 0xFF);
    return res;
  }

  void handleRequest(AsyncWebServerRequest *request) {
    AsyncWebServerResponse* response;
    response = request->beginResponse(MYFS, "/index.html", "text/html");
    response->addHeader("Content-Encoding", "gzip");
    request->send(response);
  };
};

#endif