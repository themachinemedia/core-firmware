#include <CaptivePortal.h>
#include <FileStream.h>
#include <WebServer.h>

/* ============== Private ============= */

/** @brief handle Not Found page */
void WebServerClass::handleNotFound(AsyncWebServerRequest *request) {
  request->redirect("/");
}

/** @brief handle Root */
void WebServerClass::handleRoot(AsyncWebServerRequest *request) {
  FileStream.stream(request, "/index.html") ? Debug.detail("Success Server -> %s", __func__) : Debug.error("Fail to serve -> %s", __func__);
}

/** @brief handle JavaScript */
void WebServerClass::handleJS(AsyncWebServerRequest *request) {
  FileStream.stream(request, "/main.js") ? Debug.detail("Success Server -> %s", __func__) : Debug.error("Fail to serve -> %s", __func__);
}

/** @brief handle CSS */
void WebServerClass::handleStyle(AsyncWebServerRequest *request) {
  FileStream.stream(request, "/main.css") ? Debug.detail("Success Server -> %s", __func__) : Debug.error("Fail to serve -> %s", __func__);
}

/** @brief handle WiFi Image */
void WebServerClass::handleWiFiImg(AsyncWebServerRequest *request) {
  FileStream.stream(request, "/wifi.png") ? Debug.detail("Success Server -> %s", __func__) : Debug.error("Fail to serve -> %s", __func__);
}

/** @brief handle Lock Image */
void WebServerClass::handleLockImg(AsyncWebServerRequest *request) {
  FileStream.stream(request, "/lock.png") ? Debug.detail("Success Server -> %s", __func__) : Debug.error("Fail to serve -> %s", __func__);
}

/* ============== Public ============= */

/** @brief Setup the WebServer */
void WebServerClass::initialize() {
  Debug.detail("Running %s", __func__);
  
  WiFi.softAP(STA_NAME);

  dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
  dnsServer.start(53, "*", WiFi.softAPIP());

  server.on("/", HTTP_GET, std::bind(&WebServerClass::handleRoot, this, _1));

  // Serving JavaScript Files
  server.on("/main.js", HTTP_GET, std::bind(&WebServerClass::handleJS, this, _1));

  // Serving Bootstrap Files
  server.on("/main.css", HTTP_GET, std::bind(&WebServerClass::handleStyle, this, _1));

  // Serving Image Files
  server.on("/wifi.png", HTTP_GET, std::bind(&WebServerClass::handleWiFiImg, this, _1));
  server.on("/lock.png", HTTP_GET, std::bind(&WebServerClass::handleLockImg, this, _1));

  server.onNotFound(std::bind(&WebServerClass::handleNotFound, this, _1));

  server.addHandler(new CaptiveRequestHandler()).setFilter(ON_AP_FILTER);

  server.begin();
}

void WebServerClass::DnsLoop() {
  dnsServer.processNextRequest();
}

WebServerClass WebServer;