#ifndef WEB_SOCKET_H
#define WEB_SOCKET_H

#include <Arduino.h>
#include <Debug.h>
#include <WebSocketsServer.h>

using namespace std::placeholders;

typedef enum {
  WS_CONNECT,
  WS_SCANNER,
  WS_PAUSED
}websocket_status_t;

class WebSocketClass {
 private:
  /* Instances */
  DebugClass Debug;
  WebSocketsServer webSocket;

  /* Variables */
  uint8_t lastInterfaceNumber;
  String lastReceivedPayload;
  bool wifiConnect = false;
  websocket_status_t status = WS_PAUSED;

  /* Methods */
  /** @brief  Create Settings Object to send over WebSocket
   *  @return Json Object as String
   */
  String createObject();

  /** @brief  Create a Network Object to send over WebSocket
   *  @return Json Object as String
   */
  String createNetworkObject();

  /** @brief  Deals with the current network status
   *          and sends a response over websocket
   *  @param  status  Status of the Wifi Connection
   */
  void handleNetworkResponse(wl_status_t status);

  /** @brief          Reads received Object
   *  @param  object  String of the object
   */
  void readReceivedObject(String object);

  /** @brief handle webSocketEvent */
  void webSocketEvent(uint8_t num, WStype_t type, uint8_t* payload, size_t lenght);

 public:
  /* Variables */
  bool client;

  /* Constructor */
  WebSocketClass() : Debug("WebSocket"), webSocket(81) {
    // this->webSocket = new WebSocketsServer(81);
    // this->webSocket->begin();
    // this->webSocket->onEvent(std::bind(&WebSocketClass::webSocketEvent, this, _1, _2, _3, _4));
  };

  /* Methods */
  /** @brief start WebSocket */
  void initialize();

  /** @brief access webSocket loop */
  void loop();

  /** @brief          Send response back from the WS server
   *  @param  status  Network Status of the board
   */
  void sendResponse(wl_status_t status);

  String getSettingsObject();

  String getNetworkRequest();
};

extern WebSocketClass WebSocket;

#endif