#include <ArduinoJson.h>
#include <FileManager.h>
#include <Network.h>
#include <WebSocket.h>

/* =========== Private =========== */

/** @brief  Create Settings Object to send over WebSocket
 *  @return Json Object as String
 */
String WebSocketClass::createObject() {
  DynamicJsonDocument doc(1024);
  JsonObject root = doc.to<JsonObject>();

  JsonObject wifi = root.createNestedObject("wifi");
  wifi["ssid"] = "network_name";
  wifi["password"] = "password1";
  wifi["connected"] = true;
  wifi["signal_strngth"] = 3;

  String payload;
  serializeJson(doc, payload);
  serializeJsonPretty(doc, Serial);

  return payload;
}

/** @brief  Create a Network Object to send over WebSocket
 *  @return Json Object as String
 */
String WebSocketClass::createNetworkObject() {
  StaticJsonDocument<2048> doc;
  JsonObject root = doc.to<JsonObject>();

  std::vector<scanned_networks_t> ScannedNetworkVector = Network.getScannedNetworks();

  JsonArray nested = root.createNestedArray("ScannedNetworks");

  int index = 0;
  for (auto i = ScannedNetworkVector.begin(); i != ScannedNetworkVector.end(); i++) {
    JsonObject wifi = nested.createNestedObject();
    wifi["SSID"] = ScannedNetworkVector[index].SSID;
    wifi["RSSI"] = ScannedNetworkVector[index].RSSI;
    wifi["Protected"] = !ScannedNetworkVector[index].open;
    index++;
  }

  String payload;
  serializeJson(doc, payload);
  serializeJsonPretty(doc, Serial);
  return payload;
}

/** @brief  Deals with the current network status
 *          and sends a response over websocket
 *  @param  status  Status of the Wifi Connection
 */
void WebSocketClass::handleNetworkResponse(wl_status_t status) {
  Serial.println(status);
  switch (status) {
    case 1:
      Debug.warn("Sending SSID Not Found to Interface");
      Debug.detail("Interface - > %d", lastInterfaceNumber);
      webSocket.sendTXT(lastInterfaceNumber, "SSID Not Found");
      break;

    case 3:
      Debug.warn("Sending Connected to Interface");
      Debug.detail("Interface - > %d", lastInterfaceNumber);
      webSocket.sendTXT(lastInterfaceNumber, "Connected");
      break;

    case 4:
      Debug.warn("Sending Wrong Password to Interface");
      Debug.detail("Interface - > %d", lastInterfaceNumber);
      webSocket.sendTXT(lastInterfaceNumber, "Wrong Password");
      break;

    default:
      Debug.warn("Sending Wrong Password to Interface");
      Debug.detail("Interface - > %d", lastInterfaceNumber);
      webSocket.sendTXT(lastInterfaceNumber, "Wrong Password");
      break;
  }
}

void WebSocketClass::readReceivedObject(String object) {
  String title;
  StaticJsonDocument<1024> doc;

  auto err = deserializeJson(doc, object);

  if (err) {
    Debug.error(F("deserializeJson() Fail with code %s"), err.c_str());
    return;
  }

  title = doc["title"].as<String>();
  Debug.error("%s", title.c_str());

  if (title.equals("WiFiConnect")) {
    scanned_networks_t currentNetwork;
    Debug.warn("WiFI Connect Command received");
    currentNetwork.SSID = doc["SSID"].as<String>();
    currentNetwork.Password = doc["PWD"].as<String>();
    Network.updateCredentials(currentNetwork);
    status = WS_CONNECT;
    return;
  } 

}

/** @brief handle webSocketEvent */
void WebSocketClass::webSocketEvent(uint8_t num, WStype_t type, uint8_t* payload, size_t lenght) {
  String payload_str = String((char*)payload);
  lastInterfaceNumber = num;
  switch (type) {
    case WStype_DISCONNECTED:  // if the websocket is disconnected
      Debug.detail("[%u] Disconnected!", num);
      client = false;
      break;
    case WStype_CONNECTED: {  // if a new websocket connection is established
      IPAddress ip = webSocket.remoteIP(num);
      Debug.detail("[%u] Connected from %d.%d.%d.%d url: %s", num, ip[0], ip[1], ip[2], ip[3], payload);
      client = true;
    } break;
    case WStype_TEXT:  // if new text data is received
      Debug.detail("[%u] get Text: %s", num, payload);
      if (payload_str.equals("Data Request")) {
        Debug.detail("Data Request Received");
        webSocket.sendTXT(num, createObject().c_str());
      } else if (payload_str.equals("Network Request")) {
        Debug.detail("Network Request");
        status = WS_SCANNER;
      } else if (payload[0] == '{') {
        Debug.detail("Object Received");
        Debug.warn("%s", payload_str.c_str());
        readReceivedObject(payload_str);
      }
      break;

    default:
      Debug.warn("Unhandle Type . %d", type);
      break;
  }
}

/* =========== Public =========== */

/** @brief start WebSocket */
void WebSocketClass::initialize() {
  Debug.detail("Running %s", __func__);
  webSocket.begin();
  webSocket.onEvent(std::bind(&WebSocketClass::webSocketEvent, this, _1, _2, _3, _4));
}

/** @brief access webSocket loop */
void WebSocketClass::loop() {
  switch (status) {
    case WS_SCANNER:
      webSocket.sendTXT(lastInterfaceNumber, createNetworkObject().c_str());
      status = WS_PAUSED;
      break;

    case WS_CONNECT:
      Network.connect();
      handleNetworkResponse(WiFi.status());
      status = WS_PAUSED;
      break;

    default:
      break;
  }
  webSocket.loop();
}

/** @brief          Send response back from the WS server
 *  @param  status  Network Status of the board
 */
void WebSocketClass::sendResponse(wl_status_t status) {
  handleNetworkResponse(status);
}

String WebSocketClass::getSettingsObject() {
  return createObject();
}

String WebSocketClass::getNetworkRequest() {
  return createNetworkObject();
}

WebSocketClass WebSocket;