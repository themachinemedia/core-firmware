#include <FileStream.h>

/* ============== Private ========== */

/**
 * @brief           check if the file exists
 * @param[in] path  to the file 
 */
bool FileStreamClass::isFile(String path) {
  if (MYFS.exists(path)) {
    Debug.detail(F("File %s Found"), path.c_str());
    return true;
  } else {
    Debug.error(F("File %s is Missing"), path.c_str());
    return false;
  }
}

/* ============== Public ========== */

/**
 * @brief   Setup  the FileStream
 * @return  true if FS is mounted
 */
bool FileStreamClass::initialize() {
  if (MYFS.begin(true)) {
    Debug.detail(F("MYFS Started"));
    return true;
  } else {
    Debug.detail(F("MYFS Fail"));
    return false;
  }
} 

/**
 * @brief                 Stream a file depending on it's extension
 * @param[out]  request   Request Input from server
 * @param[in]   path      to file gzip: compacted?
 */
bool FileStreamClass::stream(AsyncWebServerRequest* request, String path) {
  if (isFile(path)) {
    File file = MYFS.open(path, "r");
  } else {
    Debug.error(F("Fail to Server File"));
    return false;
  }
  AsyncWebServerResponse* response;

  if (path.endsWith(".js")) {
    response = request->beginResponse(MYFS, path, "application/javascript");
  } else if (path.endsWith(".css")) {
    response = request->beginResponse(MYFS, path, "text/css");
  } else if (path.endsWith(".html")) {
    response = request->beginResponse(MYFS, path, "text/html");
  } else if (path.endsWith(".png")) {
    response = request->beginResponse(MYFS, path, "img/png");
    response->addHeader("Expires","Mon, 1 Jan 2222 10:10:10 GMT");
  } else if (path.endsWith(".jpg")) {
    response = request->beginResponse(MYFS, path, "img/jpeg");
  } else if (path.endsWith(".json")) {
    response = request->beginResponse(MYFS, path, "application/json");
  } else {
    Debug.error("Unknown File Type");
    return false;
  }

  #ifdef FS_GZIP
    response->addHeader("Content-Encoding", "gzip");
  #endif

  request->send(response);
  return true;
}

FileStreamClass FileStream;