#ifndef FileStream_H
#define FileStream_H

#include <Arduino.h>
#include <Debug.h>
#ifdef ESP8266
#include <ESPAsyncTCP.h>
#elif defined(ESP32)
#include <AsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>

#include <FS.h>

#ifdef USE_LITTLEFS
#ifdef ESP8266
#include <LittleFS.h>
#define MYFS LittleFS
#else
#include <LITTLEFS.h>
#define MYFS LITTLEFS
#endif
#else
#include <SPIFFS.h>
#define MYFS SPIFFS
#endif

class FileStreamClass {
 private:
  /* Instances */
  AsyncWebServer* server;
  DebugClass Debug;

  /* Methods */

  /**
   * @brief           check if the file exists
   * @param[in] path  to the file 
   */
  bool isFile(String path);

 public:
  /* Constructor */
  FileStreamClass() : Debug("FileStream") { this->server = new AsyncWebServer(80); }

  /**
   * @brief Setup  the FileStream
   * @return  true if FS is mounted
   */
  bool initialize();

  /**
   * @brief                 Stream a file depending on it's extension
   * @param[out]  request   Request Input from server
   * @param[in]   path      to file gzip: compacted?
   */
  bool stream(AsyncWebServerRequest* request, String path);
};

extern FileStreamClass FileStream;

#endif