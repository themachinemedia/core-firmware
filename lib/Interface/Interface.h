#ifndef INTERFACE_H
#define INTERFACE_H

#include <Arduino.h>
#include <WebServer.h>
#include <WebSocket.h>

class InterfaceClass : public WebServerClass, WebSocketClass {
 private:
 public:
  /* Methods */
  void initialize() {
    WebServerClass::initialize();
    WebSocketClass::initialize();
    client = true;
    while (client) {
      loop();
      delay(0);
    }
  }

  void loop() {
    if (client) {
      WebSocketClass::loop();
      DnsLoop();
    }
  }
};

extern InterfaceClass Interface;

#endif