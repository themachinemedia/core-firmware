#ifndef MQTT_H
#define MQTT_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Debug.h>
#include <Module.h>
#include <PubSubClient.h>
#include <WiFi.h>

struct mqtt_settings {
  const char* device_name;
  const char* user;
  const char* password;
  const char* server;
  uint16_t port;
};

struct mqtt_publish_msg {
  const char* topic;
  const char* msg;
};

class MQTTClass : public DebugClass {
  /* Static */
 public:
  /** @brief            Callback void function used by the MQTT client
   *                    to handle what was received
   *  @param  topic     Topic of message
   *  @param  payload   Message's content
   *  @param  length    Size of message
   */
  static void callback(char* topic, byte* payload, unsigned int length);

  void subscribeToAllModules();

 private:
#ifdef UNIT_TEST
 public:
#endif
  /* Variable */
  bool status = false;

  /* Instances */
  WiFiClient wifiClient;
  PubSubClient MqttClient;
  mqtt_settings Settings;

  /* Methods */

  /** @brief Auto Discovery JSON Encoding */
  void autodiscovery();

  /** @brief                Print current MQTT Settings */
  void printSettings();

 public:
  /* Instances */
  TaskHandle_t xHandler;
  QueueHandle_t msg_queue;
  SemaphoreHandle_t taskSemaphore = xSemaphoreCreateBinary();

  /* Constructor */
  MQTTClass() : DebugClass("MQTT"){};
  MQTTClass(mqtt_settings _settings_) : DebugClass("MQTT"), Settings(_settings_){};

  /* Methods */
  /** @brief                Initializes MQTT settings */
  void initialize();

  /** @brief                Overload */
  void initialize(JsonDocument& doc, JsonDocument&);

  /** @brief                Overload */
  void initialize(mqtt_settings& _settings) {
    Settings = _settings;
    printSettings();
    initialize();
  }

  /** @brief  Connect to mqtt Server
   *  @return true if connected
   */
  bool connect();

  /** @brief                Check if MQTT is connected
   *  @return               true if connected
   */
  bool isConnected() {
    return MqttClient.connected();
  }

  /** @brief  Trys to reconnect to MQTT Server */
  void reconnect();

  bool subscribe(const char* topic);

  /** @brief                Tries to publish a message to OUT_TOPIC
   *  @param    Message     struct with topic/message
   *  @returns              true if successful
   */
  bool publishOut(const mqtt_publish_msg& Message);

  mqtt_settings& getSettings() {
    return Settings;
  }

  /** @brief  PubSub Loop */
  void loop() { MqttClient.loop(); }
};

extern MQTTClass MQTT;

#endif