#include <MQTT.h>
#include <Module.h>
#ifdef UNIT_TEST
#include <unity.h>
String test_messageMe;
#endif

static DebugClass Debug("MQTT");

void vTask(void* pvParameter) {
  vTaskDelay(200);
  xSemaphoreTake(MQTT.taskSemaphore, portMAX_DELAY);
  Debug.detail("Starting MQTT task");
  mqtt_publish_msg Message;
#ifdef UNIT_TEST
  void (*test_task_running)() = []() {
    TEST_PASS();
  };
  RUN_TEST(test_task_running);
#endif
  for (;;) {
    MQTT.loop();
    if (xQueueReceive(MQTT.msg_queue, (void*)&Message, 0) == pdTRUE) {
      if(!MQTT.publishOut(Message)) {
        MQTT.connect();
      }
      xSemaphoreGive(MQTT.taskSemaphore);
    }
  }
}

/* ============= Static ============== */

static char* subStr(char* input_string, char* separator, int segment_number) {
  char *act, *sub = NULL, *ptr;
  static char copy[100];
  int i;

  strcpy(copy, input_string);
  strcat(copy, separator);
  for (i = 1, act = copy; i <= segment_number; i++, act = NULL) {
    sub = strtok_r(act, separator, &ptr);
    if (sub == NULL) break;
  }
  return sub;
}

/** @brief            Callback void function used by the MQTT client
 *                    to handle what was received
 *  @param  topic     Topic of message
 *  @param  payload   Message's content
 *  @param  length    Size of message
 */
void MQTTClass::callback(char* topic, byte* payload, unsigned int length) {
  String messageMe = "";
  char separator[] = "/";

  for (int i = 0; i < length; i++) {
    messageMe += (char)payload[i];
  }

  Debug.warn("Message Arrived on topic: " LOG_COLOR(LOG_COLOR_BLUE) " [%s] :", topic);
  Debug.notice("Message : %s%s", LOG_COLOR(LOG_COLOR_GREEN), messageMe.c_str());

#ifdef UNIT_TEST
  test_messageMe = messageMe;
  static char* test_topic = topic;
  void (*test_callback_event_from_Task)() = []() {
    TEST_ASSERT_EQUAL_STRING("Task", test_messageMe.c_str());
  };

  void (*test_callback_event)() = []() {
    TEST_ASSERT_EQUAL_STRING("Testing", test_messageMe.c_str());
  };

  if (!strcmp("test/task", test_topic))
    RUN_TEST(test_callback_event_from_Task);

  if (!strcmp("test/topic", test_topic)) {
    RUN_TEST(test_callback_event);
    UNITY_END();
  }

#endif

  char* module = subStr(topic, separator, 2);
  char* action = subStr(topic, separator, 3);

  int index = Module::findModuleByName(module);

  if (index == 255) return;

  Modules[index]->executeActionByName(action, &messageMe);
}

void MQTTClass::subscribeToAllModules() {
  mqtt_settings _Settings = getSettings();
  if (isConnected()) {
    Debug.detail("Subscribing to Modules");

    for (auto it = Modules.begin(); it != Modules.end(); ++it) {
      const char* name = (*it)->getName();
      for (int i = 0; i < (*it)->getActionSize(); i++) {
        const char* action = (*it)->getActionName(i);
        char topic[100];
        sprintf(topic, "%s/%s/%s", _Settings.device_name, name, action);
        subscribe(topic);
      }
    }

  } else {
    reconnect();
  }
}

/* ============= Private ============= */

/** @brief                Tries to publish a message to OUT_TOPIC
 *  @param    Message     struct with topic/message
 *  @returns              true if successful
 */
bool MQTTClass::publishOut(const mqtt_publish_msg& Message) {
  print("Publishing to %s -> %s", Message.topic, Message.msg);
  bool status = MqttClient.publish(Message.topic, Message.msg);
  if (status)
    detail("Publish Successfully");
  else
    error("Problem Publishing");
  return status;
}

/** @brief Auto Discovery JSON Encoding */
void MQTTClass::autodiscovery() {
  char msg[300];  // JSON message

  DynamicJsonDocument root1(1024);
  root1["device_class"] = "motion";
  //root1["name"] = "Patts Sensor";
  root1["state_topic"] = "homeassistant/binary_sensor/pattSensor/state";
  serializeJson(root1, msg);  // my code

  MqttClient.publish("homeassistant/binary_sensor/pattSensor/config", msg);  //my code for mqtt discovery
}

/** @brief                Print current MQTT Settings */
void MQTTClass::printSettings() {
  if (Settings.device_name == nullptr) {
    log_e("missing device_name");
    return;
  }
  if (Settings.user == nullptr) {
    log_e("missing user");
    return;
  }
  if (Settings.password == nullptr) {
    log_e("missing password");
    return;
  }
  if (Settings.server == nullptr) {
    log_e("missing server");
    return;
  }
  if (Settings.port == 0) {
    log_e("missing port");
    return;
  }
  detail("ID: %s  |  User: %s  |  Password: %s  |  Server/Port: %s:%d",
         Settings.device_name, Settings.user, Settings.password, Settings.server, Settings.port);
}

/* ============= Public ============== */

/** @brief                Initializes MQTT settings */
void MQTTClass::initialize() {
  detail("Initializing MQTT");
  xHandler = NULL;
  MqttClient.setClient(wifiClient);

  MqttClient.setServer(Settings.server, 1883);

  MqttClient.setCallback(MQTTClass::callback);
  status = true;

  xTaskCreate(vTask, "Task", 4 * 1024, NULL, 1, &xHandler);
  msg_queue = xQueueCreate(5, sizeof(mqtt_publish_msg));

#ifdef UNIT_TEST
  static QueueHandle_t test_msg_queue = msg_queue;
  static TaskHandle_t test_xHandler = xHandler;
  static SemaphoreHandle_t test_taskSemaphore = taskSemaphore;
  void (*test_semaphore_created)() = []() {
    if (test_taskSemaphore != NULL)
      TEST_PASS();
    else
      TEST_FAIL();
  };
  RUN_TEST(test_semaphore_created);

  void (*test_task_created)() = []() {
    if (test_xHandler != NULL)
      TEST_PASS();
    else
      TEST_FAIL();
  };
  RUN_TEST(test_task_created);

  void (*test_queue_created)() = []() {
    if (test_msg_queue != NULL)
      TEST_PASS();
    else
      TEST_FAIL();
  };
  RUN_TEST(test_queue_created);
#endif
}

/** @brief                Overload */
void MQTTClass::initialize(JsonDocument& hardware, JsonDocument& docCredentials) {
  mqtt_settings _Settings;
  /* Parse JsonDocument to Object */
  JsonObject mqtt = docCredentials["mqtt"].as<JsonObject>();

  _Settings.device_name = hardware["id"].as<const char*>();
  _Settings.user = hardware["product_id"].as<const char*>();
  _Settings.password = hardware["id"].as<const char*>();
  _Settings.server = mqtt["server"].as<const char*>();
  _Settings.port = mqtt["port"].as<int>();

  Settings = _Settings;
  printSettings();
  initialize();
}

/** @brief  Connect to mqtt Server
 *  @return true if connected
 */
bool MQTTClass::connect() {
  if (!status) {
    error("MQTT Settings are not defined");
    return false;
  }
  if (WiFi.status() != WL_CONNECTED) {
    error("Board is not connected to the network");
    return false;
  }

  if (!MqttClient.connected()) {
    warn("Connecting to MQTT Server %s", Settings.server);

    if (MqttClient.connect(Settings.device_name, Settings.user, Settings.password)) {
      print("Connected to MQTT Server at : %s | %s");
      xSemaphoreGive(taskSemaphore);
      // autodiscovery();
    } else {
      error("Failed to Connect to MQTT");
      return false;
    }
  }

  return true;
}

/** @brief  Trys to reconnect to MQTT Server */
void MQTTClass::reconnect() {
  if (!connect()) {
    error("Restarting the Board, MQTT FAILED");
    ESP.restart();
  }
}

bool MQTTClass::subscribe(const char* topic) {
  bool status;
  status = MqttClient.subscribe(topic);
  Debug.notice("Subscribing to Topic : %s | %s", topic, status ? LOG_COLOR(LOG_COLOR_GREEN) "Success" : LOG_COLOR(LOG_COLOR_RED) "FAILED");
  return status;
}

MQTTClass MQTT;