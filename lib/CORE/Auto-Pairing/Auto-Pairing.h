#ifndef _AUTO_PARING_H_
#define _AUTO_PARING_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Network.h>
#include <Debug.h>

#ifndef TCP_PORT
#define TCP_PORT 9000
#endif

class AutoParringClass : public DebugClass {
 private:
  /* Varaibles */
  IPAddress TCPAddr;
  wifi_credentials_t SharedCredentials;

  /* Methods */
  void parseResponse(const String& response);

  void connect();

  /** @brief                Starts the Auto-paring */
  void initialize();

  /** @brief                Get the Gateway Address */
  void setTCPAddr();

  /** @brief                fetch server credentials
   *  @param      response  from the server
   */
  void fetchCredentials(String& response);

 public:
  /* Constructor */
  AutoParringClass() : DebugClass("Auto-Paring") {
    initialize();
  };

  wifi_credentials_t getCredentials() {return SharedCredentials;}
};

#endif