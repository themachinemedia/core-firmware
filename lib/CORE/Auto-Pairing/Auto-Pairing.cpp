#include <Auto-Pairing.h>

/* =========== Private ========= */

void wifiScanner() {
  Serial.println("scan start");

  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("scan done");
  if (n == 0) {
    Serial.println("no networks found");
  } else {
    Serial.print(n);
    Serial.println(" networks found");
    for (int i = 0; i < n; ++i) {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
}

void AutoParringClass::parseResponse(const String& response) {
  warn("Parsing Received Informations");
  StaticJsonDocument<512> doc;

  auto err = deserializeJson(doc, response);
  if (err) {
    error(F("deserializeJson() failed with code %s"), err.c_str());
    return;
  }

  SharedCredentials.SSID = doc["ssid"].as<String>();
  SharedCredentials.PSK = doc["psk"].as<String>();

  notice("SSID : %s \n PSK : %s", SharedCredentials.SSID.c_str(), SharedCredentials.PSK.c_str());

}

void AutoParringClass::connect() {
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  wifiScanner();
  WiFi.begin("raspi", "raspberry");
  if (WiFi.waitForConnectResult() == WL_CONNECTED) {
    print("Board is Connected to HUB");
  } else {
    error("Fail to connect ");
    return;
  }
}

/** @brief                Starts the Auto-paring */
void AutoParringClass::initialize() {
  notice("Initializing the Auto-Paring");

  /* Connects to Hub */
  while (WiFi.status() != WL_CONNECTED) {
    connect();
  }

    /* Update TCP Server Address */
    setTCPAddr();

  String response;
  fetchCredentials(response);

  parseResponse(response);
}

/** @brief                Get the Gateway Address */
void AutoParringClass::setTCPAddr() {
  warn("Getting TCP Server Address");

  TCPAddr = WiFi.gatewayIP();

  notice("TCP Server Address is %s", TCPAddr.toString().c_str());
}

/** @brief                fetch server credentials
 *  @param      response  from the server
 */
void AutoParringClass::fetchCredentials(String& response) {
  WiFiClient client;
  warn("Connecting to TCP Server");

  if (!client.connect(TCPAddr, TCP_PORT)) {
    error("Connection Failed");
    return;
  }

  print("Connection Stablished");

  client.print("requestCredentials\n");

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      error(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  while (client.available()) {
    response += (char)client.read();
  }

  notice("Acquired response : %s", response.c_str());
}