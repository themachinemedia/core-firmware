#ifndef SPIFLASH_H
#define SPIFLASH_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include <StreamUtils.h>
#include <Debug.h>
#include <SPI.h>
#include <SerialFlash.h>

/*Definition of the CS pin for ESP32.*/
#define MEM_CS 5

class SpiFlashChip : public DebugClass {
 private:
  /* Varaibles */
  bool status = false;

  /* Methods */
  bool begin();

  void format();

  bool openNSave(const char* path, SerialFlashChip& fs, const String& content);

  bool saveToFlash(const char* path, SerialFlashChip& fs, const String& content);

  void readFile(const char* path, SerialFlashChip& fs,  String& response);

  void showFolders();

 public:
  SpiFlashChip() : DebugClass("SPI"){};

  bool initialize(bool shouldFormat);

  bool getSettings(const char* path, JsonDocument& doc);

  bool saveSettings(const char* path, JsonDocument& doc);
};

extern SpiFlashChip FlashChip;

#endif