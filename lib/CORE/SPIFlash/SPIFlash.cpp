#include <SPIFlash.h>

/* ========= Private ========== */

/** @brief  Starts Flash Chip
 *  @return true if chip was initialized
 */
bool SpiFlashChip::begin() {
  uint8_t chipID[8];
  if (!SerialFlash.begin(MEM_CS)) {
    error("Unable to Access Chip");
    SerialFlash.readID(chipID);
    print("Serial Flash was initialized Properly | Chip ID -> %hhu%hhu%hhu", chipID[0], chipID[1], chipID[2]);
    return false;
  }
  SerialFlash.readID(chipID);
  print("Serial Flash was initialized Properly | Chip ID -> %hhu%hhu%hhu", chipID[0], chipID[1], chipID[2]);
  return true;
}

void SpiFlashChip::format() {
  detail("Formatting the IC");
  SerialFlash.eraseAll();
  unsigned long dotMillis = millis();
  unsigned char dotcount = 0;
  while (SerialFlash.ready() == false) {
    if (millis() - dotMillis > 1000) {
      dotMillis = dotMillis + 1000;
      Serial.print(".");
      dotcount = dotcount + 1;
      if (dotcount >= 60) {
        Serial.println();
        dotcount = 0;
      }
    }
  }
  detail("IC is 100%% Clean");
}

bool SpiFlashChip::openNSave(const char* path, SerialFlashChip& fs, const String& content) {
  SerialFlashFile file = fs.open(path);
  detail("Content to be save %s", content.c_str());
  
    if (file) {
      detail("File Open");
      file.erase();
      int writeLen = file.write(content.c_str(), content.length());
      notice("write len %d", writeLen);
      if(file.getWriteError()) {
        error("Write error : %d", file.getWriteError());
      }
      file.close();

      file = fs.open(path);
      String readContent = file.readString();
      print("COntent of the file -> %s", readContent.c_str());
      file.close();
      
      print("File Saved");
      return true;
    } else {
      error("Fail to openfile");
    }
    return false;

}

bool SpiFlashChip::saveToFlash(const char* path, SerialFlashChip& fs, const String& content) {
  if(SerialFlash.exists(path)) {
    print("File already exists Opening it");
    openNSave(path, fs, content);
    return true;
  } else if (SerialFlash.create(path, content.length())) {
    print("File has been created on FLASH");
    
  } else {
    error("Fail to create the file");
  }
  return false;
}

void SpiFlashChip::readFile(const char* path, SerialFlashChip& fs , String& response) {
  uint32_t fileSize;

  if (SerialFlashFile file = fs.open(path)) {
    detail("File is open to read");
    fileSize = file.size();
    detail("File size -> %d", fileSize);
    response = file.readString();
    warn("Finish read");
    print("COntent of the file -> %s", response.c_str());
  } else {
    error("Fail to open the file");
  }
}

void SpiFlashChip::showFolders() {
  SerialFlash.opendir();
  while (1) {
    char filename[64];
    uint32_t filesize;

    if (SerialFlash.readdir(filename, sizeof(filename), filesize)) {
      print("File ->  %s | Size: %d bytes", filename, filesize);
    } else {
      detail("End of the Folder");
      break;  // no more files
    }
  }
}

/* ========= Public ========== */

/** @brief Initializes Flash Chip */
bool SpiFlashChip::initialize(bool shouldFormat) {
  if (begin()) {
    detail("SPI Flash Chip Initialized Successfully");
    status = true;
    if (shouldFormat)
      format();
    showFolders();
    return true;
  }
  return false;
}

bool SpiFlashChip::getSettings(const char* path, JsonDocument& doc) {
  if (!status) {
    detail("Flash not initialized yet");
    return false;
  }
  if (!SerialFlash.exists(path)) {
    error("No %s File found on FLASH memory", path);
  }

  String response;
  readFile(path, FS_OBJECT, response);

  auto err = deserializeJson(doc, response);
  if (err) {
    error("deserializeJson() Faield with code %s", err.c_str());
    return false;
  }

  serializeJson(doc, Serial);

  return true;
}

bool SpiFlashChip::saveSettings(const char* path, JsonDocument& doc) {
  if (!status) {
    detail("Flash not initialized yet");
    return false;
  }
  String content;

  serializeJson(doc, content);

  if (saveToFlash(path, FS_OBJECT, content)) {
    print("Settings Saved into flash");
    return true;
  } else {
    error("Fail to save settings to flash");
  }

  return false;
}

SpiFlashChip FlashChip;