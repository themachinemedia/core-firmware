#include <Clock.h>
#include <Debug.h>
#include <Network.h>
#include <WiFiUdp.h>

WiFiUDP ntpUDP;

/* ============ Static ========== */

/** @brief Prints the time in a digital clock format */
void ClockClass::printHour() {
  Serial.println(Clock.getTimeStr(now()));
}

/* ============ Private ========== */

/** @brief               Returns a string formated as Digital Clock
  *  @param[in]   moment  The current timestamp
  *  @return              String already formated
  */
String ClockClass::getTimeStr(time_t moment) {
  char timeStr[50];
  sprintf(timeStr, "%02d:%02d:%02d", hour(moment), minute(moment), second(moment));
  return timeStr;
}

/* ============ Public =========== */

/** @brief Set up the Clock*/
void ClockClass::initialize() {
  /* Set time to zero */
  setTime(0, 0, 0, 0, 0, 0);

  /* Print time once every second */
  Alarm.timerRepeat(PRINT_HOUR_FREQUENCY, ClockClass::printHour);

}

ClockClass Clock;