#ifndef CLOCK_H
#define CLOCK_H

#include <Arduino.h>
#include <Debug.h>
#include <TimeAlarms.h>

#ifndef PRINT_HOUR_FREQUENCY
#define PRINT_HOUR_FREQUENCY 30
#endif

class ClockClass : public DebugClass {
 public:
  /* Statics */
  /** @brief Prints the time in a digital clock format */
  static void printHour();

 private:
  /* Methods */
  /** @brief               Returns a string formated as Digital Clock
  *  @param[in]   moment  The current timestamp
  *  @return              String already formated
  */
  String getTimeStr(time_t moment);

 public:
  /* Constructor */
  ClockClass() : DebugClass("Clock"){};

  /* Methods */
  /** @brief Set up the Clock*/
  void initialize();
};

extern ClockClass Clock;
#endif