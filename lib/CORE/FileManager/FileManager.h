#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

#ifdef USE_LITTLEFS
#ifdef ESP8266
#include <LittleFS.h>
#define MYFS LittleFS
#else
#include <LITTLEFS.h>
#define MYFS LITTLEFS
#endif
#else
#include <SPIFFS.h>
#define MYFS SPIFFS
#endif

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Debug.h>
#include <StreamUtils.h>

#define OBJECT_SIZE 512

class FileSystemClass : DebugClass {
 private:
  /* Methods */

  /** @brief        Open Specific File
   *  @param  file  Object passed by reference
   *  @param  path  to the file
   *  @param  mode  "w" for write
   */
  bool openFile(File& file, const String& path, const char* mode = "");

  /** @brief       Prints the content of a Dir
   *  @param  dir  path of a dir
   */
  void openDir(const char* dir);

  /** @brief        Checks if the file exists
   *  @param path   to the file
   *  @param print  Should print the result //$$ Prob Not necessary anymore!
   *  @return       true Success, false Failure
   */
  bool checkFile(const String& path, bool _print_ = false);

 public:
  FileSystemClass() : DebugClass("FileSystem"){};

  /** @brief Starts File System */
  bool setup();



  /** @brief        Writes String input into a file
   *  @param input  to be writen into file
   *  @param path   to the file
   *  @return       true Success, false Failure
   */
  bool writeFile(const String& input, String path);

  /** @brief      Writes a Json Object into a SPIFFS File
   *  @param doc  Json Object
   *  @param path to the file
   *  @return     true Success, false Failure 
   */
  bool writeFileJSON(const JsonDocument& doc, String& path);

  /** @brief        Read a Json Object from MYFS File
   *  @param  doc   Json Object
   *  @param  path  to the file
   *  @return       true if Successful
   */
  bool readFileJSON(JsonDocument& doc, String& path);

  /** @brief      Read a file
   *  @param path to the file
   *  @return     String with the content
   */
  bool readFile(String& payload, String& path);
};

extern FileSystemClass FileSystem;

#endif