#include <FileManager.h>

/* =========== Private ========= */

/** @brief        Open Specific File
 *  @param  file  Object passed by reference
 *  @param  path  to the file
 *  @param  mode  "w" for write
 */
bool FileSystemClass::openFile(File& file, const String& path, const char* mode) {
  file = MYFS.open(path, mode);
  if (!file) {
    error("Fail to open %s ", path.c_str());
    return false;
  } else {
    detail("%s is open", path.c_str());
    return true;
  }
}

/** @brief       Prints the content of a Dir
 *  @param  dir  path of a dir
 */
void FileSystemClass::openDir(const char* dir) {
  File root = MYFS.open(dir);

  File file = root.openNextFile();
  String str = "";
  while (file) {
    str += file.name();
    str += " / ";
    str += file.size();
    Serial.println(str.c_str());
    str = "";

    file = root.openNextFile();
  }
}

/** @brief        Checks if the file exists
 *  @param path   to the file
 *  @param print  Should print the result //$$ Prob Not necessary anymore!
 *  @return       true Success, false Failure
 */
bool FileSystemClass::checkFile(const String& path, bool _print_) {
  if (MYFS.exists(path)) {
    if (_print_) {
      //print(F("File %s Found"), path.c_str());
    }
    return true;
  } else {
    if (_print_) {
      warn(F("File %s Missing"), path.c_str());
    }
    return false;
  }
}

/* =========== Public ========== */

/** @brief Starts File System */
bool FileSystemClass::setup() {
  if (MYFS.begin(true)) {
    print(F("====<<< Building MYFS  >>>>>>===\n"));
    print(F("MYFS Started"));
    openDir("/");
    print(F("\r\n"));
    return true;
  } else {
    error(F("MYFS Start Fail"));
    return false;
  }
}



/** @brief        Writes String input into a file
 *  @param input  to be writen into file
 *  @param path   to the file
 *  @return       true Success, false Failure
 */
bool FileSystemClass::writeFile(const String& input, String path) {
  print(F("-> Saving %s"), path.c_str());
  File file;

  if (!openFile(file, path, "w")) {
    file.close();
    return false;
  }

  file.print(input);
  file.close();
  return true;
}

/** @brief      Writes a Json Object into a MYFS File
 *  @param doc  Json Object
 *  @param path to the file
 *  @return     true Success, false Failure 
 */
bool FileSystemClass::writeFileJSON(const JsonDocument& doc, String& path) {
  print(F("Saving %s"), path.c_str());
  File file = MYFS.open(path, "w");

  if (!openFile(file, path, "w")) {
    file.close();
    return false;
  }

  serializeJson(doc, file);
  file.close();
  return true;
}

/** @brief        Read a Json Object from MYFS File
 *  @param  doc   Json Object
 *  @param  path  to the file
 *  @return       true if Successful
 */
bool FileSystemClass::readFileJSON(JsonDocument& doc, String& path) {
  detail("Reading %s as Json", path.c_str());

  if (FileSystem.checkFile(path, true)) {
    File file;
    if (!openFile(file, path, "r")) {
      return false;
    }

    String payload = file.readString();
    file.close();

    auto err = deserializeJson(doc, payload);
    if (err) {
      detail(F("deserializeJson() failed with code %s"), err.c_str());
      return false;
    }

  } else {
    warn(F("File: %s | Not Found"), path.c_str());
    return false;
  }

  return true;
}

/** @brief      Read a file
 *  @param path to the file
 *  @return     String with the content
 */
bool FileSystemClass::readFile(String& payload, String& path) {
  print(F("Reading File : %s"), path.c_str());
  if (checkFile(path, true)) {
    File file;
    if (!openFile(file, path, "r")) {
      return false;
    }

    payload = file.readString();
    file.close();
  } else {
    warn(F("File: %s | Not Found"), path.c_str());
    return false;
  }
  return true;
}

FileSystemClass FileSystem;
