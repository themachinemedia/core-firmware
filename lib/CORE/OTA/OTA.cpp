#include <ArduinoJson.h>
#include <OTA.h>
#include <Update.h>
#include <WiFi.h>

/* ============ Private =========== */

/** @brief          Callback function
 *  @param[in]  ret return result of the updater
 */
void OTAClass::cbOtaUpdate(const t_httpUpdate_return ret) {
  switch (ret) {
    case HTTP_UPDATE_FAILED:
      error("HTTP_UPDATE_FAILED Error (%d): %s", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
      break;

    case HTTP_UPDATE_NO_UPDATES:
      warn("HTTP_UPDATE_NO_UPDATES");
      break;

    case HTTP_UPDATE_OK:
      warn("UPDATE SUCCESSFUL! RESTARTING THE BOARD");
      ESP.restart();
      break;
  }
}

void OTAClass::update(Stream& updateSource, size_t updateSize) {
  if (Update.begin(updateSize)) {
    size_t written = Update.writeStream(updateSource);

    if (written == updateSize) {
      print("Written : %d  successfully", written);
    } else {
      error("Written only : %d | Expected : %d ", written, updateSize);
    }
    if (Update.end()) {
      detail("Update Done!");
      if (Update.isFinished()) {
        print("Update successfully completed. Rebooting.");
        ESP.restart();
      } else {
        error("Update Fails");
      }
    } else {
      error("Error Occurred. Error #: %d ", Update.getError());
    }

  } else {
    error("Not enough space to begin OTA");
  }
}

/** @brief      Perform an Update From the FileSystem
 *  @param  fs  FileSytem Used (SD_MMC)
 */
void OTAClass::updateFromFS(SerialFlashChip& fs) {
  /* Open File inside FileSystem */
  SerialFlashFile updateBin = fs.open("/OTA.bin");

  /* Check if File is Open */
  if (updateBin) {
    /* Grab the File Size */
    size_t updateSize = updateBin.size();

    /* Check if File is not Empty */
    if (updateSize > 0) {
      warn("Try to start update");
      update(updateBin, updateSize);
    } else {
      error("Error, file is empty");
    }

    /* Close File */
    updateBin.close();

    // whe finished remove the binary from sd card to indicate end of the process
    fs.remove("/OTA.bin");
  } else {
    error("Could not load OTA.bin from File System");
  }
}

/** @brief          Saves the Bin File asa SD Card
 *  @param[in]  OTA Object with OTA informations
 */
bool OTAClass::saveToFS(const ota_information_t& OTA, SerialFlashChip& fs) {
  HTTPClient http;
  String path = "/OTA.bin";
  for (int i = 0; i < 3; i++) {
    http.begin(OTA.url);
    int httpCode = http.GET();
    detail("HTTP Code -> %d", httpCode);
    if (httpCode == HTTP_CODE_OK) {
      warn("Connected and Downloading file from server");
      error("SIZE -> %d", http.getSize());
      if (SerialFlash.create(path.c_str(), http.getSize())) {
        print("Bin file was successfuly created");
        SerialFlashFile file = fs.open("/OTA.bin");
        if (file) {
          detail("Bin File was open");
          http.writeToStream(&file);
          file.close();
          print(".bin File Downloaded");
          return true;
        } else {
          error("Fail to open Bin file");
        }
      } else {
        error("Fail to create the file");
      }
    } else {
      error("Fail to conntact Server");
    }
  }
  return false;
}

/** @brief          Update over OTA
 *  @param[in]  url target url of the BIN
 */
void OTAClass::getUpdate(const String& url) {
  WiFiClient client;
  print(F("\n\n====================<<<<<<<<  Updating  >>>>>>=====================\n\n"));

  print("%s", url.c_str());

  print("Current Version on the Board : %s", String(FIRMWARE_VERSION));
  httpUpdate.rebootOnUpdate(false);
  t_httpUpdate_return ret = httpUpdate.update(client, url);
  cbOtaUpdate(ret);
}

/** @brief            Contact server and try to get the Version information.
 *  @param[out]  OTA  Object with OTA informations
 */
bool OTAClass::contactServer(ota_information_t& OTA) {
  HTTPClient http;
  warn("Trying to contact -> %s", OTA.url.c_str());
  for (int i = 0; i < 3; i++) {
    http.begin(OTA.url);
    int httpCode = http.GET();
    detail("HTTP Code -> %d", httpCode);
    if (httpCode == HTTP_CODE_OK) {
      OTA.payload = http.getString();
      http.end();
      detail("Payload %s", OTA.payload.c_str());
      return true;
    } else {
      error("Fail to reach Server");
      http.end();
    }
  }
  return false;
}

/** @brief                  Get the version Available in the server
 *  @param[out]  payload    from the server
 */
bool OTAClass::getVersionAvailable(ota_information_t& OTA) {
  StaticJsonDocument<2048> doc;

  auto err = deserializeJson(doc, OTA.payload);
  if (err) {
    error("deserializeJson() Fail with code %s", err.c_str());
    return false;
  }

  OTA.version = doc["version"].as<String>();
  OTA.url = SERVER_ADDRESS "/" + doc["path"].as<String>();

  return true;
}

/* ============ Public =========== */

/** @brief          Verifies Server URL 
 *  @param[out] OTA Object with OTA informations
 *  @return         true if there is a new version
 */
bool OTAClass::isVersionUpdated(ota_information_t& OTA) {
  StaticJsonDocument<512> doc;
  detail("Current Version ->>>> %s", FIRMWARE_VERSION);
  OTA.url = SERVER_ADDRESS "/version.json";

  if (contactServer(OTA)) {
    if (getVersionAvailable(OTA)) {
      if (OTA.version.equals(FIRMWARE_VERSION)) {
        print("Code is Updated\r\n");
      } else {
        warn("New Version found.\r\n");
        return false;
      }
    }
  }
  return true;
}

/** @brief        Updates the board using HTTP
 *  @param[in]   url  URL to bin file
 */
void OTAClass::updateHTTP(const String& url) {
  getUpdate(url);
}

/** @brief  Updates the OTA into a file inside SD Card
 *  @param[in] OTA Object with OTA informations
 */
bool OTAClass::updateToSDCard(const ota_information_t& OTA) {
  if(SerialFlash.exists("/OTA.bin")) {
    warn("File already Exists");
    return true;
  }
  return saveToFS(OTA, FS_OBJECT);
}

void OTAClass::updateFromSDCard() {
  updateFromFS(FS_OBJECT);
}

OTAClass Updater;