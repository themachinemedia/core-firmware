#ifndef OTA_H
#define OTA_H

#include <Arduino.h>
#include <Debug.h>
#include <FS.h>
#include <HTTPUpdate.h>
#include <SerialFlash.h>

#ifndef SERVER_ADDRESS
#define SERVER_ADDRESS "http://192.168.1.58:3000"
#endif

struct ota_information_t {
  String url;
  String version;
  String payload;
};

class OTAClass : public DebugClass {
 private:
  /* Methods */

  /** @brief          Callback function
   *  @param[in]  ret return result of the updater
   */
  void cbOtaUpdate(const t_httpUpdate_return ret);

  void update(Stream& updateSource, size_t updateSize);

  /** @brief      Perform an Update From the FileSystem
   *  @param  fs  FileSytem Used (SD_MMC)
   */
  void updateFromFS(SerialFlashChip& fs);

  /** @brief          Saves the Bin File on SD Card
   *  @param[in]  OTA Object with OTA informations
   */
  bool saveToFS(const ota_information_t& OTA, SerialFlashChip& fs);

  /** @brief          Update over OTA
   *  @param[in]  url target url of the BIN
   */
  void getUpdate(const String& url);

  /** @brief            Contact server and try to get the Version information.
   *  @param[out]  OTA Object with OTA informations
   */
  bool contactServer(ota_information_t& OTA);

  /** @brief                  Get the version Available in the server
   *  @param[out]  payload    from the server
   */
  bool getVersionAvailable(ota_information_t& OTA);

 public:
  /* Constructor */
  OTAClass() : DebugClass("Updater"){};

  /* Methods */

  /** @brief          Verifies Server URL 
   *  @param[out] OTA Object with OTA informations
   *  @return         true if there is a new version
   */
  bool isVersionUpdated(ota_information_t& OTA);

  /** @brief        Updates the board using HTTP
   *  @param[in]   url  URL to bin file
   */
  void updateHTTP(const String& url);

  /** @brief  Updates the OTA into a file inside SD Card
   *  @param[in] OTA Object with OTA informations
   */
  bool updateToSDCard(const ota_information_t& OTA);

  void updateFromSDCard();
};

extern OTAClass Updater;

#endif