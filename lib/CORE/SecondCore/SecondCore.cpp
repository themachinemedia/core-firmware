#include <Clock.h>
#include <Debug.h>
#include <SecondCore.h>
#include <TimeAlarms.h>

TaskHandle_t second_core_handle;
DebugClass DebugSecond("Second Core");

static void SecondCoreLoop(void* pvParameters);

/** @brief Second Core setup() equivalent */
void SecondCoreSetup() {
  Serial.print("Arduino running on core -> ");
  Serial.println(xPortGetCoreID());
  xTaskCreatePinnedToCore(SecondCoreLoop, "SC", 4096, NULL, 1, &second_core_handle, 0);
}

/** @brief Second Core loop() equivalent */
static void SecondCoreLoop(void* pvParameters) {
  DebugSecond.print("Second Core running on core - > %d", xPortGetCoreID());

  /* Initlizes Clock */
  Clock.initialize();

  for (;;) {
    Alarm.delay(500);
    vTaskDelay(10);
  }
}