#include <IPAddress.h>
#include <Network.h>
#include <esp_wifi.h>

static DebugClass Debug("Network");

static void wifiInit() {
  wifi_mode_t mode;
  if (esp_wifi_get_mode(&mode) == ESP_ERR_WIFI_NOT_INIT) {
    Debug.detail("Initializing WiFi");
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    esp_err_t err = esp_wifi_init(&cfg);
    ESP_ERROR_CHECK(err);
  }
}

/* ========== Private =========== */

/** @brief Print IP */
void NetworkClass::printIp() {
  char buf[16];
  IPAddress ip = WiFi.localIP();
  sprintf(buf, "%u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);
  print(F("Local Ip : %s "), buf);
}

/** @brief  Check if there is a SSID Set in the board already
 *  @return true if there is
 */
bool NetworkClass::isSSIDSet() {
  wifi_config_t conf;
  esp_err_t err;

  wifiInit();

  err = esp_wifi_get_config(WIFI_IF_STA, &conf);
  ESP_ERROR_CHECK(err);

#ifdef UNIT_TEST
  detail("SSID Saved on the Board: %s", reinterpret_cast<char*>(conf.sta.ssid));
#endif

  if (String(reinterpret_cast<char*>(conf.sta.ssid)).equals("")) {
    return false;
  } else {
    return true;
  }
}

/**
 * @brief   Scan available networks and fill an vector
 * @return  the vector filled
 */
std::vector<scanned_networks_t> NetworkClass::scan() {
  std::vector<scanned_networks_t> ScannedNetworkVector;

  int n = WiFi.scanNetworks();

  for (int i = 0; i < n; i++) {
    scanned_networks_t ScannedNetwork;

    ScannedNetwork.SSID = WiFi.SSID(i);
    ScannedNetwork.RSSI = getRSSIasQuality(WiFi.RSSI(i));
    ScannedNetwork.open = (WiFi.encryptionType(i) == WIFI_AUTH_OPEN);
    ScannedNetworkVector.push_back(ScannedNetwork);
  }

  return ScannedNetworkVector;
}

/**
 * @brief         Parse RSSI dBm signal to Quality (%)
 * @param   RSSI  Signal strenght in dBm
 * @return        Signal as Quality (%)
 */
int NetworkClass::getRSSIasQuality(int RSSI) {
  int quality = 0;

  if (RSSI <= -100) {
    quality = 0;
  } else if (RSSI >= -50) {
    quality = 100;
  } else {
    quality = 2 * (RSSI + 100);
  }
  return quality;
}

/** @brief                Print Connection Result */
void NetworkClass::printNetworkResult() {
  const char* result = "";
  switch (WiFi.status()) {
    case 1:
      result = LOG_COLOR_NORMAL(COLOR_MAGENTA) "SSID Not Found";
      break;

    case 3:
      result = LOG_COLOR_NORMAL(COLOR_GREEN) "Connected";
      break;

    case 4:
      result = LOG_COLOR_NORMAL(COLOR_RED) "Connection Failed";
      break;

    case 5:
      result = LOG_COLOR_NORMAL(COLOR_RED) "Connection Lost";
      break;

    case 6:
      result = LOG_COLOR_NORMAL(COLOR_RED) "Disconnected";
      break;
    default:
      break;
  }

  warn("Connection Result: %s", result);
}

/** @brief  Attempt to connect using the Private Credentials
 *  @return true if Successful
 */
bool NetworkClass::privateConnect() {
  wifiInit();

   /* This will verify if a pre-build variable was set */
  if (strcmp(NETWORK_SSID, "")) {
    use_build_flags = true;
  }
  
  wifi_config_t conf;
  esp_err_t err = esp_wifi_get_config(WIFI_IF_STA, &conf);
  ESP_ERROR_CHECK(err);

  if (use_build_flags) {
    notice("Using the credentials defined on Platformio.ini File");
    SSID = NETWORK_SSID;
    Password = NETWORK_PWD;
  }

  strcpy(reinterpret_cast<char*>(conf.sta.ssid), SSID.c_str());
  strcpy(reinterpret_cast<char*>(conf.sta.password), Password.c_str());

  if (esp_wifi_set_config(WIFI_IF_STA, &conf)) {
    error("WiFi Settings Failed");
  }

  return attemptToConnect();
}

bool NetworkClass::attemptToConnect() {
  warn(" Attempting to Connect " LOG_COLOR_NORMAL(COLOR_MAGENTA) "%s > " LOG_COLOR_NORMAL(COLOR_GRAY) "%s", SSID.c_str(), Password.c_str());
  WiFi.begin();
  if (WiFi.waitForConnectResult()) {
    printNetworkResult();
  }

  if (WiFi.status() == WL_CONNECTED) {
    printIp();
    return true;
  } else {
    error(F("Fail to reconnect"));
    wifi_config_t conf;
    memset(&conf, 0, sizeof(wifi_config_t));
    if (esp_wifi_set_config(WIFI_IF_STA, &conf)) {
      detail("clear config failed!");
    }
    return false;
  }
}

/** @brief  Connect to a already stablished SSID and Passwrod 
 *  @return true if WiFi Connected 
 */
bool NetworkClass::reconnect() {
  if (!isSSIDSet()) {
    error("No SSID Settings Found");
    return false;
  }

  warn("Attempting to reconnect to Wifi");
  WiFi.begin();

  return attemptToConnect();
}

/* ========== Public ============ */

/**
 * @brief   Get all the available Networks scanned in a vector
 * @return  Vector of scanned networks
 */
std::vector<scanned_networks_t> NetworkClass::getScannedNetworks() {
  std::vector<scanned_networks_t> ScannedNetworkVector = scan();

  int index = 0;

  for (auto i = ScannedNetworkVector.begin(); i != ScannedNetworkVector.end(); i++) {
    print("%.2d : SSID : %s | RSSI : %d | %s",
          index,
          ScannedNetworkVector[index].SSID.c_str(),
          ScannedNetworkVector[index].RSSI,
          ScannedNetworkVector[index].open ? "Open" : "Protected");
    index++;
  }

  return ScannedNetworkVector;
}

/** @brief                Connects to WiFi
 *  @param  Credentials   New Network Credentials
 */
void NetworkClass::updateCredentials(scanned_networks_t Credentials) {
  warn("Updating Credentials -> %s | %s ", Credentials.SSID.c_str(), Credentials.Password.c_str());

  SSID = Credentials.SSID;
  Password = Credentials.Password;
}

/** @brief                Connects to WiFi
 *  @param  Credentials   New Network Credentials
 */
void NetworkClass::updateCredentials(wifi_credentials_t Credentials) {
  warn("Updating Credentials -> %s | %s ", Credentials.SSID.c_str(), Credentials.PSK.c_str());

  SSID = Credentials.SSID;
  Password = Credentials.PSK;
}

/** @brief  Check if the board has internet access and reconnect if needed
 *  @return true if connected to WiFi
 */
bool NetworkClass::isConnected() {
  if (WiFi.status() == WL_CONNECTED) {
    return true;
  }
  if (!isSSIDSet()) {
    return false;
  }

  return reconnect();
}

/** @brief  Private Connect */
void NetworkClass::connect() {
  privateConnect();
}

NetworkClass Network;