#ifndef NETWORK_H
#define NETWORK_H

#include <Arduino.h>
#include <Debug.h>
#include <WiFi.h>

#include <vector>

#ifndef NETWORK_SSID
#define NETWORK_SSID ""
#endif

#ifndef NETWORK_PWD
#define NETWORK_PWD ""
#endif

typedef struct scanned_networks_t {
  String SSID;
  String Password;
  int32_t RSSI;
  bool open;
} scanned_networks_t;

struct wifi_credentials_t {
  String SSID;
  String PSK;
};

typedef enum {
  /* Status to do Nothing */
  NTW_PAUSED = 255,

  /* Status to Attempt connection after interface requests it */
  NTW_PRIVATE_CONNECT = 0,

  /* Check if the Board has a saved network and if it is connected */
  NTW_CHECK_CONNECTION = 1
} network_status_t;

class NetworkClass : public DebugClass {
 private:
#ifdef UNIT_TEST
 public:
#endif
  /* Variables */
  String SSID;
  String Password;
  network_status_t status = NTW_PAUSED;
  bool use_build_flags = false;

  /* Methods */
  /** @brief Print IP */
  void printIp();

  /** @brief  Check if there is a SSID Set in the board already
   *  @return true if there is
   */
  bool isSSIDSet();

  /**
   * @brief   Scan available networks and fill an vector
   * @return  the vector filled
   */
  std::vector<scanned_networks_t> scan();

  /**
   * @brief         Parse RSSI dBm signal to Quality (%)
   * @param   RSSI  Signal strenght in dBm
   * @return        Signal as Quality (%)
   */
  int getRSSIasQuality(int RSSI);

  /** @brief                Print Connection Result */
  void printNetworkResult();

  /** @brief  Attempt to connect using the Private Credentials
   *  @return true if Successful
   */
  bool privateConnect();

  bool attemptToConnect();

  /** @brief  Connect to a already stablished SSID and Passwrod 
   *  @return true if WiFi Connected 
   */
  bool reconnect();

 public:
  /* Constructor */
  NetworkClass() : DebugClass("Network"){};

  /* Methods */
  /**
   * @brief   Get all the available Networks scanned in a vector
   * @return  Vector of scanned networks
   */
  std::vector<scanned_networks_t> getScannedNetworks();

  /** @brief                Connects to WiFi
   *  @param  Credentials   New Network Credentials
   */
  void updateCredentials(scanned_networks_t Credentials);

  /** @brief                Connects to WiFi
   *  @param  Credentials   New Network Credentials
   */
  void updateCredentials(wifi_credentials_t Credentials);

  /** @brief  Check if the board has internet access and reconnect if needed
   *  @return true if connected to WiFi
   */
  bool isConnected();

  /** @brief  Private Connect */
  void connect();
};

extern NetworkClass Network;

#endif