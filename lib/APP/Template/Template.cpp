#include <Template.h>

int TemplateClass::ID = 0;

/* ========== Private =========== */

/** @brief                Set Hardware Settings
 *  @param      object    Json Object Containing the Settings
 */
void TemplateClass::setHardwareSettings(const JsonObject& object) {
  /* If the module has any hardware configuration, this function should be used to build it. */
}

/** @brief                Link Actions to functions */
void TemplateClass::setDefaultActions() {
  /* [ ] This might change! how the Default actions should be set */
    String msg;
  {
    module_function_t function;
    function.name = "doOne";
    msg += function.name;
    function.pointer = std::bind(&TemplateClass::doOne, this);
    functionPointer.push_back(function);
  }
  msg += ", ";
  {
    module_function_t function;
    function.name = "doTwo";
    msg += function.name;
    function.pointer = std::bind(&TemplateClass::doTwo, this);
    functionPointer.push_back(function);
  }

  detail("Setting Actions %s", msg.c_str());
}

/** @brief                Initialize Template APP */
void TemplateClass::initialize() { 
  /* Perform any initialization necessary here */
}

/* << Actions >> */
void TemplateClass::doOne() {
  /* This is an action to be performed thru the MQTT */
}

void TemplateClass::doTwo() {
  /* This in another action to be performed thru the MQTT */
}

/* ========== Public =========== */

/** @brief                Set all the settings according tho the Json Object
 *  @param    hardware    Json Object in SPIFFS
 *  @param    preferences Preferences Object in SPIFFS
 *  @details              Required in modules
 */
void TemplateClass::setSettings(JsonObject& hardware, JsonObject& preferences) {
  warn("Setting Template Module as : %s", Identity.module_name);

  /* Start by setting the Actions */
  setDefaultActions();

  /* If Hardware settings are needed, try setting them before initializing the module */
  setHardwareSettings(hardware);
  /* Same for Preferences */
  // setPreferences(preferences);

  /* Initialize the Modules */
  initialize();
  
}