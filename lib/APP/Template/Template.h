#ifndef _TEMPLATE_H_
#define _TEMPLATE_H_

/* Add Libraries here */
#include <Arduino.h>
#include <Module.h>

/* Declare Structers here */
struct template_pin_assignment_t {
  int pin1;
  int pin2;
};

class TemplateClass : protected Module {
 public:
  /* Static Methods */
  static void* createNewInstance() {
    return new TemplateClass;
  }

 private:
  /* Static Variables */
  static int ID;
  std::string newName = "template";

  /* Instances */

  /* Methods */

  /** @brief                Set Hardware Settings
   *  @param      object    Json Object Containing the Settings
   */
  void setHardwareSettings(const JsonObject& object);

  /** @brief                Link Actions to functions */
  void setDefaultActions();

  /** @brief                Initialize LED APP */
  void initialize();

  /* << Actions >> */
  void doOne();
  void doTwo();

 public:
  /* Constructor */
  TemplateClass() : DebugClass("Template") {
    newName += String(ID).c_str();
    Identity.module_name = newName.c_str();
    ID++;
  }

  /** @brief                Set all the settings according tho the Json Object
   *  @param    hardware    Json Object in SPIFFS
   *  @param    preferences Preferences Object in SPIFFS
   *  @details              Required in modules
   */
  void setSettings(JsonObject& hardware, JsonObject& preferences);
};

#endif