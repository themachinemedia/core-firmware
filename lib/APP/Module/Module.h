#ifndef _MODULES_H_
#define _MODULES_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include <Debug.h>

#include <memory>
#include <vector>

using namespace std::placeholders;

/* Struct to hold all available APP's and their instanciate functions */
struct app_settings_t {
  const char* library_name;
  const char* name;
  void* (*app_instance_func)();
  app_settings_t(const char* _lib_name, const char* _name, void* (*func)());
};

struct module_function_t {
  std::function<void(void*)> pointer;
  const char* name;
};

struct module_identity_t {
  const char* module_name;
};

class Module : virtual public DebugClass {
 private:
#ifdef UNIT_TEST
 public:
#endif
  /* Static Variables */
  static int modules_size;
  static int modules_sizeP;

  /* Methods */
  static Module* setup(JsonObject& SettingsObject);

 protected:
  /* Instances */
  std::vector<module_function_t> functionPointer;
  module_identity_t Identity = {.module_name = "module"};

  /** @brief                Set all the settings according tho the Json Object
   *  @param    object      Json Object in SPIFFS
   *  @details              Required in modules
   */
  virtual void setSettings(JsonObject& objectHardware, JsonObject& objectPreferences) {
    error("Module Base has no settings to be set!!");
  }

 public:
  /* Constructor */
  Module() : DebugClass("Modules"){};

  /** @brief                Read the settings and build all the necessary modules
   *  @param      doc       Document with all the modules array
   */
  static bool initialize(JsonDocument& doc, JsonDocument& docHardware);

  /** @brief                Seach a module with a matching name in the vector
   *  @param      _name     of the module to be found
   */
  static int findModuleByName(const char* _name);

  /** @brief                Run a function in the array based on the index
   *  @param      index     of the function in the vector
   *  @param      _args     of action
   */
  bool executeActionByIndex(int index, void* _args) {
    try {
      functionPointer.at(index).name;
    } catch(const std::out_of_range& oor) {
      error("Invalid Action Index");
      return false;
    }
    notice("Executing action with index %d", index);
    functionPointer.at(index).pointer(_args);
    return true;
  }

  /** @brief                Return the size of function's vector
   *  @return               size
   */
  int getActionSize() {
    return functionPointer.size();
  }

  /** @brief                Return Action's name by index */
  const char* getActionName(int index) {
    try {
      functionPointer.at(index).name;
    } catch(const std::out_of_range& oor) {
      error("Invalid Action Index");
      return NULL;
    }
    return functionPointer.at(index).name;
  }

  /** @brief                Returns the Module's Name
   *  @return               Module's name
   */
  const char* getName() { return Identity.module_name; }

  /** @brief                execute an action by it's name
   *  @param      _name     of the action
   *  @param      _args     of action
   *  @returns              action's index on vector
   *  @details              nonStatic and nonvirtual function
   */
  int executeActionByName(const char* _name, void* _args);
};

extern std::vector<Module*> Modules;
extern std::vector<app_settings_t> Apps;

/* ========== Non Methods =========== */

/** @brief                Parse Array to vector
 *  @param        key     name of Array
 *  @param        object  Json Object
 *  @param        vector  to save information on
 *  @return               true if vector was parsed
 */
template <typename T>
bool toVector(const char* key, JsonObject& object, std::vector<T>& vector) {
  /* Create Vector and check if it was created*/
  JsonArray array = object[key].as<JsonArray>();
  if (!array) return false;

  /* Save each element of array to vector */
  for (JsonVariant v : array) {
    vector.push_back(v);
  }

  /* Check if the size of the array matches the vector size */
  if (vector.size() == array.size()) return true;

  return false;
}

/** @brief                Print Array Content
 *  @param        name    of Array
 *  @param        vecotr  to be printed
 */
template <typename T>
void printArray(const char* name, std::vector<T>& vector) {
  Serial.printf("\033[0;33m%s [ ", name);

  /* Check if type is integer */
  bool isInt = typeid(int) == typeid(T);

  /* Print all Elements in the vector */
  for (auto it = vector.begin(); it != vector.end() - 1; ++it) {
    if (isInt)
      Serial.printf("%d, ", *it);
    else
      Serial.printf("%s, ", *it);
  }

  /* Print the final one */
  if (isInt)
    Serial.printf("%d", vector.back());
  else
    Serial.printf("%s, ", vector.back());
  Serial.print("]\033[0m\n");
}

#endif