#include <Module.h>

#include <typeinfo>

/* Initialize a Static variable on Modules class to keep track of how many modules we currently have */
int Module::modules_size = 0;
int Module::modules_sizeP = 0;
static DebugClass Debug("Modules");
std::vector<Module*> Modules;
std::vector<app_settings_t> Apps;

/** @brief                Find and element in a vector
 *  @param      instance  the vector to search in
 *  @param      func      function to test against
 *  @returns              the index of matching object
 */
template <typename T, typename F>
static int findInVector(std::vector<T>& instance, F func) {
  auto it = find_if(instance.begin(), instance.end(), func);

  if (it != instance.end()) {
    auto index = std::distance(instance.begin(), it);
    return index;
  } else {
    return 255;
  }
}

/* ======== Static Constructors ===== */

/** @brief                Constructor for App Settings Struct
 *  @details              Constructor function for the struct that
 *                        will hold the App's information and attempt
 *                        to add the structure inside the Apps Vector.
 */
app_settings_t::app_settings_t(const char* _lib_name, const char* _name, void* (*func)()) {
  this->library_name = _lib_name;
  this->name = _name;
  this->app_instance_func = func;
  if (findInVector<app_settings_t>(Apps, [_name](const app_settings_t& obj) {
        return !strcmp(_name, obj.name);
      }) == 255) {
    Apps.push_back((*this));
  }
}

/* ========== Private =========== */

/** @brief                Setup a module
 *  @param SettingsObject holding the Module library information
 *  @returns              the pointer of the new module
 */
Module* Module::setup(JsonObject& SettingsObject) {
  const char* lib_name = SettingsObject["library"].as<const char*>();

  int index = findInVector<app_settings_t>(Apps, [&lib_name](const app_settings_t& obj) {
    return !strcmp(lib_name, obj.library_name);
  });

  if (index != 255) {
    app_settings_t AppSettings = Apps.at(index);
    return static_cast<Module*>(AppSettings.app_instance_func());
  }

  Debug.error("Invalid Module");
  return nullptr;
}

/* ========== Public =========== */

/** @brief                Read the settings and build all the necessary modules
 *  @param    hardware    information to build the App
 *  @param    preferences [ ] not yet implemented
 */
bool Module::initialize(JsonDocument& hardware, JsonDocument& preferences) {
  /* Parse JsonDocument to Array */
  JsonArray moduleHardware = hardware["module"].as<JsonArray>();
  JsonArray modulePreferences = preferences["modules"].as<JsonArray>();
  /* Get the number of modules inside the main array */
  modules_size = moduleHardware.size();
  modules_sizeP = modulePreferences.size();
  Debug.notice("Number of Modules -> %d", modules_size, modules_sizeP);

  for (int i = 0; i < modules_size; i++) {  // *** Open Q: Do we have to check for the module size of Preferences file too?

    JsonObject objectHw = moduleHardware[i].as<JsonObject>();
    JsonObject objectPr = modulePreferences[i].as<JsonObject>();

    Module* ptr = setup(objectHw);
    if (ptr == nullptr) continue;

    //Module* ptrP = setup(objectPr);
    //if (ptrP == nullptr) continue;

    ptr->setSettings(objectHw, objectPr);
    Modules.push_back(ptr);
  }
}

/** @brief                Seach a module with a matching name in the vector
 *  @param      _name     of the module to be found
 */
int Module::findModuleByName(const char* _name) {
  int index = findInVector<Module*>(Modules, [&_name](Module* obj) {
    return !strcmp(_name, obj->getName());
  });

  if (index != 255) {
    Debug.detail("%s Moduel Found at index %d", _name, index);
    return index;
  }

  Debug.error("Module was not found!");
  return 255;
}

/** @brief                execute an action by it's name
 *  @param      _name     of the action
 *  @param      _args     of action
 *  @returns              action's index on vector
 *  @details              nonStatic and nonvirtual function
 */
int Module::executeActionByName(const char* _name, void* _args) {
  int index = findInVector<module_function_t>(functionPointer, [&_name](const module_function_t& obj) {
    return !strcmp(_name, obj.name);
  });

  if (index != 255) {
    detail("%s Action Found at index %d", _name, index);
    module_function_t function = functionPointer.at(index);
    function.pointer(_args);
    return index;
  }

  error("Action was not found!");
  return 255;
}
