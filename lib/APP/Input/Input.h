#ifndef INPUT_H
#define INPUT_H

#include <Arduino.h>
#include <Debug.h>
#include <Module.h>

struct input_settings {
  uint8_t pin_definition;
};

using namespace std::placeholders;

class InputClass : protected Module {
 private:
  /* Variables */

  /* Methods */
  /** @brief                Initialize Input APP */
  void initialize();

 public:
  /* Constructor */
  InputClass() : DebugClass("Input"){
    Identity.module_name = "input";
  };

  /* Methods */

  /** @brief                Return the pointer of a new instance
   *  @returns              pointer to the new instance
   */
  static Module* getMorph() {
    return new InputClass;
  }

  /** @brief                Set all the settings according tho the Json Object
   *  @param    object      Json Object in SPIFFS
   *  @details              Required in modules
   */
  void setSettings(JsonObject& object);
};

#endif