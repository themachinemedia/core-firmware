#include <Input.h>

Module* instanciateInputClass() {
  return InputClass::getMorph();
}

/* ============ Private ========== */
void InputClass::initialize() {
  
}

/* ============ Public =========== */

/** @brief                Set all the settings according tho the Json Object
 *  @param    object      Json Object in SPIFFS
 *  @details              Required in modules
 */
void InputClass::setSettings(JsonObject& object) {
  /* Initialize the Input APP here */
  initialize();
}