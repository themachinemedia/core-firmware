#ifndef _LED_H_
#define _LED_H_

#include <Arduino.h>
#include <Debug.h>
#include <Module.h>
#include <TLC59108.h>
#include <Wire.h>

#define DISPLAY_ERROR false  // I2C added
#define I2C_ADDR TLC59108::I2C_ADDR::BASE

struct led_core_pin_assignment_t {
  int i2c_data;
  int i2c_clock;
  int reset_pin;
  const char* addr_base;
};

struct led_library_definitions_t {
  const char* library;
  led_core_pin_assignment_t pin;
};

struct led_calibration_t {
  std::vector<int> pinWavelength;
  std::vector<int> pinBrightness;
};

struct led_settings_t {
  led_library_definitions_t module;
};

struct led_preferences_t {
  std::vector<int> rgb_color;
  int brightness;
  int fade;
  led_calibration_t calibration;
};

void vTask(void* pvParameter);

class LEDClass : public Module {
  // friend void vTaskDelay(void* pvParameter);

 public:
  /* Static Methods */
  static void* createNewInstance() {
    return new LEDClass;
  }

 private:
#ifdef UNIT_TEST
 public:
#endif
  /* Static Variables */
  static int ID;

  /* Variables */
  bool status = false;

  /* Instances */
  TLC59108 leds;
  led_library_definitions_t* Settings;
  led_preferences_t* Preferences;
  std::string newName = "light";

  /* Methods */
  // void vTask(void* pvParameter);

  /** @brief                Check if the TLC59108 can be found in the set address
   *  @returns              true if found
   */
  bool checkI2CAddress();

  /** @brief                Set Hardware Settings
   *  @param      object    Json Object Containing the Settings
   */
  void setHardwareSettings(JsonObject& object);

  /** @brief                Link Actions to functions */
  void setDefaultActions();

  /** @brief                Set Default Settings
   *  @param      object    Json Object Containing the Settings
   */
  void setPreferences(JsonObject& object);

  /** @brief                Print Hidden Configurations */
  void printHiddenConfig();

  /** @brief                Print Defualt Configurations */
  void printDefaultConfig();

  /** @brief                Initialize LED APP */
  void initialize();

  bool parseArgument(void* _args, char*& buf);

  /** @brief                Action to turn on the LED */
  void on(void* _args);

  /** @brief                Action to turn off the LED */
  void off(void* _args);

 public:
  /* Instances */
  TaskHandle_t xHandler;
  SemaphoreHandle_t taskSemaphore = xSemaphoreCreateBinary();

  /* Contructor */
  LEDClass() : DebugClass("LED"), leds(Wire, I2C_ADDR) {
    newName += String(ID).c_str();
    Identity.module_name = newName.c_str();
    ID++;
  };

  /* Methods */

  /** @brief                Set all the settings according tho the Json Object
   *  @param    hardware    Json Object in SPIFFS
   *  @param    preferences Preferences Object in SPIFFS
   *  @details              Required in modules
   */
  void setSettings(JsonObject& hardware, JsonObject& preferences);

  bool getState();
};

#endif