# Module Configuration :

### Name: 
> "name":"value"

* "name" - For the LED module to work, the value must be `light`

### Hidden Config:
> "hidden_config":{*object*}

* "library" - specifies the library | `tlc59116`
* "core_pin_assignment" - Defines the core pins on hardware.
  ```
  {
    # I2C Data pin
    "i2c_data": (int)
    # I2C Clock pin
    "i2c_clock": (int)
  }
  ```
  
* "pin_wavelength" - Defines an array of Integers (Patt)
  ```
  [
  (int)
  ]
  ```
* "pin_brigthness" - Defines an array of Integers (Patt)
  ```
  [
  (int)
  ]
  ```

### Action:
> "action":[*arrayOfChar**]

* "action" - Defines an array of possible functions
  ```
  [
  (char*)
  ]
  ```
### State:
> "state":[*arrayOfChar*]

* "state" - Defines an array of elements to report by MQTT
  ```
  (char*)
  ```
### Default Settings:
> "default_settings":{*object*}

* "default_settings - Defines the default settings
  ```
  {
    # RBG default value
    "rgb_color":[int, int, int],
    # Level of brightness
    "brightness": [int],
    # Fade time
    "fade": [int]
  }
  ```

# Example
```
  {
      "name": "light",
      "hidden_config": {
        "library": "tlc59116",
        "core_pin_assignment": {
          "i2c_data": 22,
          "i2c_clock": 23
        },
        "pin_wavelength": [ 0, 550, 455, 8, 0, 12],
        "pin_brightness": [ 0, 88, 90, 6, 2, 15]
      },
      "action": ["on", "off"],
      "state": ["brightness", "rgb_color"],
      "default_settings": {
        "rgb_color": [255,255,255],
        "brightness": [80],
        "fade": [2000]
      }
    }
```
