#include <LED.h>
#include <MQTT.h>

#include <typeinfo>
#ifdef UNIT_TEST
#include <unity.h>
#endif

int LEDClass::ID = 0;

/* ========== Private =========== */

void LED_vTask(void* pvParameter) {
  vTaskDelay(200);

  LEDClass* Instance = static_cast<LEDClass*>(pvParameter);
  DebugClass Debug(Instance->getName());
  xSemaphoreTake(MQTT.taskSemaphore, portMAX_DELAY);
  Debug.detail("Starting LED Task : %s", Instance->getName());
  xSemaphoreGive(MQTT.taskSemaphore);
#ifdef UNIT_TEST
  void (*test_task_running)() = []() {
    TEST_PASS();
  };
  RUN_TEST(test_task_running);
#endif

  while (1) {
    // xSemaphoreTake(Instance->taskSemaphore, portMAX_DELAY);
    vTaskDelay(500);
    Debug.detail("Status %s", Instance->getState() ? LOG_COLOR(COLOR_GREEN) "Active" : LOG_COLOR(COLOR_RED) "Unactive");
    mqtt_settings _Settings = MQTT.getSettings();
    char* topic;
    asprintf(&topic, "%s/%s/state", _Settings.device_name, Instance->getName());
    mqtt_publish_msg Message;
    Message.topic = topic;
    Message.msg = Instance->getState() ? "ON" : "OFF";
    xQueueSend(MQTT.msg_queue, (void*)&Message, 10);
    xSemaphoreTake(MQTT.taskSemaphore, portMAX_DELAY);
    free(topic);
  }
}

/** @brief                Check if the TLC59108 can be found in the set address
 *  @returns              true if found
 */
bool LEDClass::checkI2CAddress() {
  detail("Checking I2C Address");

  /* Starts I2C and Check if device is found*/
  Wire.beginTransmission(I2C_ADDR);
  if (!Wire.endTransmission()) {
    print("TLC59108 Found at Address: 0x%s", String(I2C_ADDR, HEX));
  } else {
    error("Fail to found TLC59108 at Address: 0x%s", String(I2C_ADDR, HEX));
    return false;
  }
  return true;
}

/** @brief                Set Hardware Settings
 *  @param      object    Json Object Containing the Settings
 */
void LEDClass::setHardwareSettings(JsonObject& object) {
  notice("Setting Library Definitions");

  /* Assign Core pins */
  JsonObject libraryVariables = object["pin_definitions"];
  Settings->pin.i2c_data = libraryVariables["i2c_data"];
  Settings->pin.i2c_clock = libraryVariables["i2c_clock"];
  Settings->pin.reset_pin = libraryVariables["reset_pin"];
  Settings->pin.addr_base = libraryVariables["addr_base"].as<const char*>();

  printHiddenConfig();
}

/** @brief                Link Actions to functions */
void LEDClass::setDefaultActions() {
  String msg;
  {
    module_function_t function;
    function.name = "on";
    msg += function.name;
    function.pointer = std::bind(&LEDClass::on, this, _1);
    functionPointer.push_back(function);
  }
  msg += ", ";
  {
    module_function_t function;
    function.name = "off";
    msg += function.name;
    function.pointer = std::bind(&LEDClass::off, this, _1);
    functionPointer.push_back(function);
  }

  detail("Setting Actions %s", msg.c_str());
}

/** @brief                Set Default Settings
 *  @param      object    Json Object Containing the Settings
 */
void LEDClass::setPreferences(JsonObject& object) {
  notice("Setting Preferences");

  /* Assing Pin Settings */

  JsonArray pin_wavelength = object["pin_wavelength"].as<JsonArray>();
  for (JsonVariant v : pin_wavelength) {
    Preferences->calibration.pinWavelength.push_back(v);
  }

  JsonArray pin_brightness = object["pin_brightness"].as<JsonArray>();
  for (JsonVariant v : pin_brightness) {
    Preferences->calibration.pinBrightness.push_back(v);
  }

  if (!toVector<int>("rgb_color", object, Preferences->rgb_color)) {
    error("Failed to parse default->rgb_color");
  }
}

/** @brief                Print Hidden Configurations */
void LEDClass::printHiddenConfig() {
  notice("Printing Config");

  /* Print core_pinAssignment Object */

  notice("pin_definitions { \n\ti2c_data -> %d, \n\ti2c_clock -> %d,\n\treset_pin -> %d }",
         Settings->pin.i2c_data,
         Settings->pin.i2c_clock,
         Settings->pin.reset_pin);
}

/** @brief                Print Defualt Configurations */
void LEDClass::printDefaultConfig() {
  notice("Printing Default Config");

  warn("default_settings {");

  Serial.print("\t");
  printArray("rgb_color", Preferences->rgb_color);
  Serial.print("\t");
  notice("brightness", Preferences->brightness);
  Serial.print("\t");
  notice("fade", Preferences->fade);

  warn("\t }");
}

/** @brief                Initialize LED APP */
void LEDClass::initialize() {
  detail("Running %s", __func__);
  detail("setting SCA as %d | SCL as %d", Settings->pin.i2c_data,
         Settings->pin.i2c_clock);

  /* Setting the I2C Based on the configs */
  Wire.begin(Settings->pin.i2c_data,
             Settings->pin.i2c_clock);

  /* Checking the I2c Address */
  if (checkI2CAddress()) {
    detail("Settings -> reset_pin : %d", Settings->pin.reset_pin);
    leds.init(Settings->pin.reset_pin);
    leds.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);
  }
  xTaskCreate(LED_vTask, "Task", 4 * 1024, this, 1, &xHandler);
}

bool LEDClass::parseArgument(void* _args, char*& buf) {
  if (_args == nullptr)
    return false;

  asprintf(&buf, LOG_COLOR(COLOR_GREEN) " | Argument :" LOG_COLOR(COLOR_MAGENTA) " %s", ((String*)_args)->c_str());

  int temp = ((String*)_args)->toInt();
  if (temp) {
    warn("Changing Brightness to %d", temp);
    Preferences->brightness = temp;
  }

  return true;
}

/** @brief                Action to turn on the LED */
void LEDClass::on(void* _args) {
  char* buf;

  print("Turning LED" LOG_COLOR(COLOR_CYAN) " ON %s", parseArgument(_args, buf) ? buf : "");
  leds.setAllBrightness((byte)Preferences->brightness);

  free(buf);
  status = true;
  xSemaphoreGive(taskSemaphore);
}

/** @brief                Action to turn off the LED */
void LEDClass::off(void* _args) {
  char* buf;

  print("Turning LED " LOG_COLOR(COLOR_RED) " OFF %s", parseArgument(_args, buf) ? buf : "");
  leds.setAllBrightness((byte)0);

  free(buf);
  status = false;
  xSemaphoreGive(taskSemaphore);
}

bool LEDClass::getState() {
  return status;
}

/* ========== Public =========== */

/** @brief                Set all the settings according tho the Json Object
 *  @param    hardware    Json Object in SPIFFS
 *  @param    preferences Preferences Object in SPIFFS
 *  @details              Required in modules
 */
void LEDClass::setSettings(JsonObject& hardware, JsonObject& preferences) {
  warn("Setting LED Module as : %s", Identity.module_name);

  Settings = new led_library_definitions_t;
  Preferences = new led_preferences_t;

  setDefaultActions();

  setHardwareSettings(hardware);
  setPreferences(preferences);

  initialize();
}