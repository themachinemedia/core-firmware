# Master - v1.7:
- LED Instance based Task Implemented.
- MQTT report state Implemented.

# To load with LED :
export PLATFORMIO_BUILD_FLAGS="-DLED_LIB"
pio run -t upload

# MQTT start Command
mosquitto -c /usr/local/etc/mosquitto

#To-Do:
- [ ] Finish Input Module.
- [x] Argument reading when mqtt publish is used.
- [x] Add State report to Modules.