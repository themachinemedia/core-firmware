const http = require('http');
const fs = require('fs');
const port = 3000;
const path = require('path');

const requestListener = (req, res) => {
  if (req.url === '/') {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    fs.readFile('files/index.html', function(error, data) {
      if (error) {
        res.writeHead(400);
        res.write('No file Found');
      } else {
        res.write(data);
      }
      res.end();
    })
  } else if (req.url.match('\.js$')) {
    var jsPath = path.join('files', req.url);
    var fileStream = fs.createReadStream(jsPath, 'UTF-8');
    res.writeHead(200, { 'Content-Type': 'text/javascript' });
    fileStream.pipe(res);
  } else if (req.url.match('\.css$')) {
    var cssPath = path.join('files', req.url);
    var fileStream = fs.createReadStream(cssPath, 'UTF-8');
    res.writeHead(200, { 'Content-Type': 'text/css' });
    fileStream.pipe(res);
  } else if (req.url.match('\.png$')) {
    console.log("Sending Image Files -> " + req.url);
    var imagePath = path.join('files', req.url);
    var fileStream = fs.createReadStream(imagePath);
    res.writeHead(200, {'Content-Type' : 'image/png',
                        'Expires': 'Mon, 1 Jan 2222 10:10:10 GMT'});
    fileStream.pipe(res);
  } else if (req.url.match('\.bin$')) {
    console.log("Sending BIN files -> " + req.url);
    var binPath  = path.join('files', req.url);
    var stats = fs.statSync(binPath);
    var fileStream = fs.createReadStream(binPath);
    res.writeHead(200, {'Content-disposition' : "attachment; filename=OTA.bin",
                        'Content-Length' : stats.size});
    fileStream.pipe(res);
  } else if(req.url.match('\.json$')) {
    console.log("Serving JSON File -> " + req.url);
    var jsonPath = path.join('files' + req.url);
    var fileStream = fs.createReadStream(jsonPath);
    res.writeHead(200, {'Content-Type' : 'application/json'});
    fileStream.pipe(res);
  }

};

const server = http.createServer(requestListener);

server.listen(port, function(error) {
  if (error) {
    console.log('Something Went Wrong');
  } else {
    console.log('Server is listen on port ' + port);
  }
})