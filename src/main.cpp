#include <Arduino.h>
#include <Auto-Pairing.h>
#include <FileManager.h>
#include <Input.h>
#include <Network.h>
#include <Module.h>
#include <MQTT.h>
#include <flash_memory.h>
#include <ota_routine.h>

#include "soc/rtc_cntl_reg.h"

static DebugClass Debug("Main");

#ifdef LED_LIB
#include <LED.h>
app_settings_t AppLED("tlc59116", "light", LEDClass::createNewInstance);
#endif

#ifdef EXAMPLE_LIB
#include <Template.h>
app_settings_t AppExample("library_Here", "App_name", TemplateClass::createNewInstance);
#endif


StaticJsonDocument<2*1024> docHardware;
StaticJsonDocument<2*1024> docCredentials;
StaticJsonDocument<2*1024> docPreferences;

String pathHardware = "/NEW-hardware-config.json";
String pathCredentials = "/NEW-secure-credentials.json";
String pathPreferences = "/NEW-user-preferences.json";

void setup() {

  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  Serial.begin(115200);
  Serial.println();

  FileSystem.setup();

  FileSystem.readFileJSON(docHardware, pathHardware);
  FileSystem.readFileJSON(docPreferences, pathPreferences);
  FileSystem.readFileJSON(docCredentials, pathCredentials);

  Module::initialize(docHardware, docPreferences);

  Network.connect();
  MQTT.initialize(docHardware, docCredentials);
  MQTT.connect();
  MQTT.subscribeToAllModules();

  while (1) {
    delay(10);
  }
  AutoParringClass AutoPairing;

  Network.updateCredentials(AutoPairing.getCredentials());
  Network.connect();

  /* Tries to connect to WiFi network */
  if (Network.isConnected()) {
    Debug.detail("Connection established without the need of running the interface");
  }

  //setMode();
}

void loop() {
 
}