#include <flash_memory.h>

static DebugClass Debug("FlashMemory");

static bool initializeFlashModules() {
  return (FileSystem.setup() &  FlashChip.initialize(FORMAT_SPI));
}

/* ========= Public =============== */

bool initializeFlashMemory() {
  if(initializeFlashModules()) {
    Debug.print("All Modules are initialized");
    return true;
  } else {
    Debug.error("Fail to initialize Flash modules");
    
  } 
  return false; 
}
