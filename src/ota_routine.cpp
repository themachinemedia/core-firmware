#include <ota_routine.h>

static ota_information_t OTA;
static DebugClass Debug("OTA/Rotuine");

/* ========== Static =========== */

static bool checkVersion() {
  if (!Updater.isVersionUpdated(OTA)) {
    Debug.detail("Version is Updated");
    return true;
  }

  return false;
}

/* ========== Public =========== */

void loadOTAfromFlash() {
  Updater.updateFromSDCard();
}

bool saveOTAonFlash() {
  if (checkVersion()) {
    return false;
  }

  return Updater.updateToSDCard(OTA);
}

bool quickInstallOTA() {
  if (checkVersion()) {
    return false;
  }
#ifdef UPDATE_AFTER_CONNECTION
  Updater.updateHTTP(OTA.url);
#endif
  return true;
}