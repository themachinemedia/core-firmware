#ifndef _OTA_ROUTINE_H_
#define _OTA_ROUTINE_H_

#include <Arduino.h>
#include <OTA.h>

/** @brief  Loads OTA from SPI Flash Chip */
void loadOTAfromFlash();

/** @brief  Saves OTA on SPI Flash Chip */
bool saveOTAonFlash();

/** @brief  Get OTA directly from HTTP */
bool quickInstallOTA();

#endif