import os

def installGit() :
  os.system("sudo apt-get update -y")
  os.system("sudo apt-get upgrade -y")
  os.system("sudo apt install git -y")
  os.system("mkdir auto-ap")
  os.chdir("/home/pi/auto-ap")
  os.system("git clone https://github.com/harshil1507/raspi-auto-ap-client.git .")
  os.chdir("/home/pi")
  os.system("sudo chown -R pi: auto-ap .*")
  os.system("curl -sSL https://get.docker.com | sh")
  os.system("sudo usermod -aG docker ${USER}")
  os.system("sudo apt-get install libffi-dev libssl-dev -y && sudo apt install python3-dev && sudo apt-get install -y python3 python3-pip && sudo pip3 install docker-compose")
  os.system("sudo systemctl enable docker")

installGit()