import os
scriptWD = ""
debug = True

# Class of different styles
class style():
    BLACK = '\033[30m'
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'

def readFile(path) :
  """ 
    Read a file
    @param    path    to the file
    @return           file's content
  """
  print(style.BLUE + 'Reading temp ' + path + ' | Path :' + style.RED + scriptWD + path)
  
  if path.count('/') > 2 :
    tempFile = open(path)
  else :
    tempFile = open(scriptWD + path)
    
  content = tempFile.read()
  tempFile.close()
  if debug:
    print(style.WHITE + content)
  return content

def setFiles() :
  os.system('touch /Volumes/boot/ssh')
  content = readFile('/scripts/wpa_supplicant.conf')
  f = open("/Volumes/boot/wpa_supplicant.conf", "w")
  f.write(content)
  f.close()

# Start by settings the working directory of the script
scriptWD = os.path.abspath(os.getcwd())
setFiles()