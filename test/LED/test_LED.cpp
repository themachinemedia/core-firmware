#include <Arduino.h>
#include <unity.h>
#ifndef UNIT_TEST
#define UNIT_TEST
#endif
#include <LED.h>
#include <Network.h>
#include <MQTT.h>

app_settings_t AppLED("tlc59116", "light", LEDClass::createNewInstance);

static DebugClass Debug("LED_Test");
StaticJsonDocument<2*1024> docHardware;
StaticJsonDocument<2*1024> docPreferences;

/* ======= Test Tools ======== */
void buildObjectHardware(JsonDocument& doc) {
  JsonArray module = doc.createNestedArray("module");

  JsonObject module_0 = module.createNestedObject();
  module_0["module_id"] = "module-1234";
  module_0["library"] = "tlc59116";
  module_0.createNestedObject("pin_definition");
  module_0["pin_definition"]["i2c_data"] = 21;
  module_0["pin_definition"]["i2c_clock"] = 22;
  module_0["pin_definition"]["reset_pin"] = 5;
  module_0["pin_definition"]["i2c_address_base"] = "0x60";
  
}

void buildObjectPreferences(JsonDocument& doc) {
  JsonArray module = doc.createNestedArray("module");

  JsonObject module_0 = module.createNestedObject();
  module_0["name"] = "Living-Room Light's Module 1";

  module_0.createNestedObject("calibration");
  JsonArray pin_wavelength = module_0["calibration"].createNestedArray("pin_wavelength");
  pin_wavelength.add(0); pin_wavelength.add(550); pin_wavelength.add(455);
  pin_wavelength.add(8); pin_wavelength.add(0); pin_wavelength.add(12);

  JsonArray pin_brightness = module_0["calibration"].createNestedArray("pin_brigthness");
  pin_brightness.add(0); pin_brightness.add(88); pin_brightness.add(90);
  pin_brightness.add(6); pin_brightness.add(2); pin_brightness.add(15);

  JsonArray rgb_color = module_0.createNestedArray("rgb_color");
  rgb_color.add(255); rgb_color.add(255); rgb_color.add(255);

  module_0["brigthness"] = 80;
  module_0["fade"] = 2000;

}

void setup() {
  Serial.begin(115200);
  delay(200);
  
  Debug.detail("Starting Test");

  UNITY_BEGIN();

  buildObjectHardware(docHardware);
  buildObjectPreferences(docPreferences);

  Module::initialize(docHardware, docPreferences);

  mqtt_settings MqttSettings = {.device_name = "ABC1234567890", .user = "machine-0001", 
  .password = "ABC1234567890", .server = "192.168.1.65", .port = 1883};

  Network.connect();
  MQTT.initialize(MqttSettings);
  MQTT.connect();
  MQTT.subscribeToAllModules();
  

  // UNITY_END();
}

void loop() {
}