#include <Arduino.h>
#include <Debug.h>
#include <unity.h>

static DebugClass Debug("Task_Test");

SemaphoreHandle_t taskSemaphore = xSemaphoreCreateBinary();

void vTask(void* pvParameter) {
  Debug.warn("Stating Task");
  void (*test_task_running)() = []() {
    TEST_PASS();
  };
  RUN_TEST(test_task_running);
  for (;;) {
    static int counter = 0;

    counter++;
    Debug.print("%d", counter);

    if (counter >= 50)
      xSemaphoreGive(taskSemaphore);

    vTaskDelay(100);
  }
}

void setup() {
  delay(200);
  Serial.begin(115200);
  UNITY_BEGIN();
  Debug.notice("Starting task Test");

  static TaskHandle_t xHandler = NULL;
  xTaskCreate(vTask, "Task", 4 * 1024, NULL, 1, &xHandler);
  configASSERT(xHandler);
  // delay(100);
  void (*test_task_created)() = []() {
    if (xHandler != NULL)
      TEST_PASS();
    else
      TEST_FAIL();
  };
  RUN_TEST(test_task_created);

  xSemaphoreTake(taskSemaphore, portMAX_DELAY);

  void (*test_task_delete)() = []() {
    vTaskDelete(xHandler);
    TEST_PASS();
  };
  RUN_TEST(test_task_delete);

  UNITY_END();
}

void loop() {
}
