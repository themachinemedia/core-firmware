#include <Arduino.h>
#include <MQTT.h>
#include <Network.h>
#include <ESP32Ping.h>
#include <FileManager.h>
#include <unity.h>

#include "soc/rtc_cntl_reg.h"

static DebugClass Debug("Test MQTT");
static MQTTClass MQTT;
static StaticJsonDocument<4096> doc;

/* Test by pinging to google.com */
void test_connection() {
  TEST_ASSERT_EQUAL(true, Ping.ping("www.google.com"));
}

/* Test if mqtt connection can be stablished */
void test_mqtt_connection() {
  TEST_ASSERT_EQUAL(true, MQTT.connect());
}

void test_SPIFFS_setup() {
  TEST_ASSERT_EQUAL(true, FileSystem.setup());
}

void test_file_read() {
  String path = "/product0001-id.json";

  FileSystem.readFileJSON(doc, path);
}

void setup() {
  delay(2000);
  UNITY_BEGIN();
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
  Serial.begin(115200);
  RUN_TEST(test_SPIFFS_setup);
  RUN_TEST(test_file_read);

  Module::initialize(doc);

  Network.connect();

  RUN_TEST(test_connection);

  MQTT.initialize(doc);
  RUN_TEST(test_mqtt_connection);

  MQTTClass::subscribeToAllModules(MQTT);

  UNITY_END();
}

void loop() {

}