#include <Arduino.h>
#ifndef UNIT_TEST
#define UNIT_TEST
#endif
#include <MQTT.h>
#include <Network.h>
#include <unity.h>

// static MQTTClass MQTT;
static DebugClass Debug("Test_MQTT");
static const char* topic = "test/topic";

void setup() {
  
  Serial.begin(115200);
  UNITY_BEGIN();
  delay(2000);
  Debug.notice("Starting MQTT Test");

  Network.connect();

  mqtt_settings MqttSettings = {.device_name = "ABC1234567890", .user = "machine-0001", 
  .password = "ABC1234567890", .server = "192.168.1.65", .port = 1883};

  MQTT.initialize(MqttSettings);

  void (*test_connection)() = []() {
    TEST_ASSERT_TRUE(MQTT.connect());
  };
  RUN_TEST(test_connection);

  void (*test_subscription)() = []()  {
    MQTT.subscribe("test/task");
    TEST_ASSERT_TRUE(MQTT.subscribe(topic));
  };
  RUN_TEST(test_subscription);
  vTaskDelay(1000);

  mqtt_publish_msg Message = {.topic = "test/task", .msg = "Task"};

  xQueueSend(MQTT.msg_queue, (void *)&Message, 10);

  vTaskDelay(1000);
  void(*test_publishing)() = []() {
    mqtt_publish_msg Message = {.topic = topic, .msg = "Testing"};
    TEST_ASSERT_TRUE(MQTT.publishOut(Message));
  };
  RUN_TEST(test_publishing);

  vTaskDelay(5000);

  void(*test_callback_event)() = []() {
    TEST_FAIL();
  };
  RUN_TEST(test_callback_event);
  UNITY_END();

}

void loop() {

}