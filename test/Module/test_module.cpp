#include <Arduino.h>
#include <unity.h>
#define UNIT_TEST
#include <Module.h>

StaticJsonDocument<2*1024> docHardware;
StaticJsonDocument<2*1024> docPreferences;
Module TestInstance;
static DebugClass Debug("Modules");


/* ======= Test Tools ======== */
void buildObjectHardware(JsonDocument& doc) {
  JsonArray module = doc.createNestedArray("module");

  JsonObject module_0 = module.createNestedObject();
  module_0["module_id"] = "module-1234";
  module_0["library"] = "tlc59116";
  module_0.createNestedObject("pin_definition");
  module_0["pin_definition"]["i2c_data"] = 21;
  module_0["pin_definition"]["i2c_clock"] = 22;
  module_0["pin_definition"]["reset_pin"] = 5;
  module_0["pin_definition"]["i2c_address_base"] = "0x60";
  
}

void buildObjectPreferences(JsonDocument& doc) {
  JsonArray module = doc.createNestedArray("module");

  JsonObject module_0 = module.createNestedObject();
  module_0["name"] = "Living-Room Light's Module 1";

  module_0.createNestedObject("calibration");
  JsonArray pin_wavelength = module_0["calibration"].createNestedArray("pin_wavelength");
  pin_wavelength.add(0); pin_wavelength.add(550); pin_wavelength.add(455);
  pin_wavelength.add(8); pin_wavelength.add(0); pin_wavelength.add(12);

  JsonArray pin_brightness = module_0["calibration"].createNestedArray("pin_brigthness");
  pin_brightness.add(0); pin_brightness.add(88); pin_brightness.add(90);
  pin_brightness.add(6); pin_brightness.add(2); pin_brightness.add(15);

  JsonArray rgb_color = module_0.createNestedArray("rgb_color");
  rgb_color.add(255); rgb_color.add(255); rgb_color.add(255);

  module_0["brigthness"] = 80;
  module_0["fade"] = 2000;

}

void setup() {
  Serial.begin(115200);
  UNITY_BEGIN();

  buildObjectHardware(docHardware);
  buildObjectPreferences(docPreferences);

  /* Method should be able to identify an invalid module. */
  void(*test_module_setup_invalid) () = []() {
    JsonArray moduleHardware = docHardware["module"].as<JsonArray>();
    JsonObject objectHw = moduleHardware[0].as<JsonObject>();
    TEST_ASSERT_EQUAL(nullptr, TestInstance.setup(objectHw));
  };
  RUN_TEST(test_module_setup_invalid);

  /* Method must be able to handle invalid initializations. */
  void (*test_module_initialization_empty)() = [](){
    TEST_ASSERT_TRUE(Module::initialize(docHardware, docPreferences));
  };
  RUN_TEST(test_module_initialization_empty);

  /* Method must be able to perform a search thru modules with an invalid key. */
  void (*test_module_find_module_by_name_empty)() = []() {
    TEST_ASSERT_EQUAL(255, Module::findModuleByName("Empty"));
  };
  RUN_TEST(test_module_find_module_by_name_empty);

  /* Method must be able to return NULL if an invalid index is given. */
  void(*test_module_get_action_name_invalid)() = []() {
    TEST_ASSERT_EQUAL_STRING(NULL, TestInstance.getActionName(255));
    
  };
  RUN_TEST(test_module_get_action_name_invalid);

  /* Method must be able to run if an invalid index is given. */
  void(*test_module_execute_action_by_index_invalid)() = []() {
    TEST_ASSERT_FALSE(TestInstance.executeActionByIndex(255, nullptr));
  };
  RUN_TEST(test_module_execute_action_by_index_invalid);

  void(*test_module_get_action_size_NULL)() = []() {
    Debug.notice("getActionSize returns : %d", TestInstance.getActionSize());
    TEST_ASSERT_EQUAL(0, TestInstance.getActionSize());
  };
  RUN_TEST(test_module_get_action_size_NULL);

  void(*test_module_get_name_empty)() = []() {
    Debug.notice("getName Method returns : %s", TestInstance.getName());
    TEST_ASSERT_EQUAL_STRING("module", TestInstance.getName());
  };
  RUN_TEST(test_module_get_name_empty);

  void(*test_module_find_action_by_name_empty)() = []() {
    TEST_ASSERT_EQUAL(255, TestInstance.executeActionByName("Empty", nullptr));
  };
  RUN_TEST(test_module_find_action_by_name_empty);

  UNITY_END();
}

void loop() {
  
}