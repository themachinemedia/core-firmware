## Module Library : **<span style="color:green"> OK </span>**
---

>test_module_setup_invalid : <span style="color:green"> PASSED </span>

    - Method must be able to identify an invalid module.

>test_module_initialization_empty : <span style="color:green"> PASSED </span>

    - Method must be able to handle invalid initializations.

>test_module_find_module_by_name_empty : <span style="color:green"> PASSED </span>

    - Method must be able to perform a search thru modules with an invalid key.

>test_module_get_action_name_invalid : <span style="color:green"> PASSED </span>

    - Method must be able to return NULL if an invalid index is given.

>test_module_execute_action_by_index_invalid : <span style="color:green"> PASSED </span>

    - Method must be able to return false if an invalid index is given.

>test_module_get_action_size_NULL : <span style="color:green"> PASSED </span>
    
    - Method must return 0 if no Module was set.

>test_module_get_name_empty : <span style="color:green"> PASSED </span>
    
    - Method must return "module" if using test instance.

>test_module_find_action_by_name_empty : <span style="color:green"> PASSED </span>

    - Method must return 255 if an invalid key is given as an argument.
