#include <Arduino.h>
#include <unity.h>
#ifndef UNIT_TEST
#define UNIT_TEST
#endif
#include <Network.h>
#include <esp_wifi.h>

static DebugClass Debug("Test_Network");

void setup() {
  
  Serial.begin(115200);
  UNITY_BEGIN();
  delay(2000);
  Debug.notice("Starting Network Test");

  WiFi.begin();
  vTaskDelay(500 / portTICK_RATE_MS);
  WiFi.disconnect(false, true);
  vTaskDelay(500 / portTICK_RATE_MS);

  void (*test_isSSIDset_with_no_SSID)() = []() {
    TEST_ASSERT_FALSE(Network.isSSIDSet());
  };
  RUN_TEST(test_isSSIDset_with_no_SSID);

  void (*test_reconnect_with_no_SSID)() = []() {
    TEST_ASSERT_FALSE(Network.reconnect());
  };
  RUN_TEST(test_reconnect_with_no_SSID);

  void (*test_connect)() = []() {
    TEST_ASSERT_TRUE(Network.privateConnect());
  };
  RUN_TEST(test_connect);

  void (*test_isSSIDset_with_SSID)() = []() {
    TEST_ASSERT_TRUE(Network.isSSIDSet());
  };
  RUN_TEST(test_isSSIDset_with_SSID);

  WiFi.disconnect();

  void (*test_reconnect_with_SSID)() = []() {
    TEST_ASSERT_TRUE(Network.reconnect());
  };
  RUN_TEST(test_reconnect_with_SSID);

  Debug.detail("Disconnecting");
  WiFi.disconnect(true, false);

  void(*test_isConnected)() = []() {
    TEST_ASSERT_TRUE(Network.isConnected());
  };
  RUN_TEST(test_isConnected);
  

  UNITY_END();
}

void loop() {
}